//
//  AppDelegate.h
//  MonitoringSystem
//
//  Created by Kang on 16/4/19.
//  Copyright © 2016年 YTYangK. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>



@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;



@end

