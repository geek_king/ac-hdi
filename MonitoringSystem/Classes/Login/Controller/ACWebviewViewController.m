//
//  ACWebviewViewController.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/21.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACWebviewViewController.h"

@interface ACWebviewViewController ()

@end

@implementation ACWebviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSURLRequest * request = [NSURLRequest requestWithURL:[NSURL URLWithString:_url]];
    UIWebView * webView = [[UIWebView alloc] initWithFrame:self.view.frame];
    webView.opaque = NO;
    webView.scalesPageToFit = YES ;
    [webView loadRequest:request];
    [self.view addSubview:webView];

}


@end
