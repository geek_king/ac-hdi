//
//  ForgetPasswordVC.m
//  DataStatistics
//
//  Created by Kang on 16/1/4.
//  Copyright © 2016年 YTYangK. All rights reserved.
// 实验git
#import "ForgetPasswordVC.h"
#import "PhoneNumber.h"
#import "NetRequestClass.h"
#import "UtilToolsClass.h"
#import "UITextField+LolitaText.h"
#define GeneralColor RGB(14, 118, 199, 1)

@interface ForgetPasswordVC () <UITextFieldDelegate, UIAlertViewDelegate> {
    
    
    CGRect frameDefault;
    int second;
    BOOL isFPVCClick;
}
@property (weak, nonatomic) IBOutlet UIButton *btnForgetPwd;

@end

@implementation ForgetPasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.btnForgetPwd setTitle:NSLocalizedString(@"确定", nil) forState:UIControlStateNormal];
    self.btnForgetPwd.enabled = NO;
    [self updateUI];
    
    self.iphoneNum.delegate = self;
    self.originalPassword.delegate = self;
    self.newpassword.delegate = self;
    
    UIBarButtonItem* backIetm = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back@3x"] style:UIBarButtonItemStylePlain target:self action:@selector(backItemClick)];
    self.navigationItem.leftBarButtonItem = backIetm;
    self.navigationItem.hidesBackButton = YES;
}

-(void)backItemClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_newpassword addTarget:self action:@selector(textLengthMethod:) forControlEvents:UIControlEventEditingChanged];
    [_originalPassword addTarget:self action:@selector(textLengthMethod:) forControlEvents:UIControlEventEditingChanged];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}


#pragma mark - 设置内容
- (void)updateUI{
    self.title = NSLocalizedString(@"重置密码", nil);
    self.FPVCguideLabel.text = NSLocalizedString(@"输入该账号的新密码", nil);
    frameDefault = self.view.frame;

  
    UITapGestureRecognizer* tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    tapGestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    _iphoneNum.textColor = [UIColor whiteColor];
    _iphoneNum.userInteractionEnabled = NO;
    [_iphoneNum addYTYanKTextFielStyle1:_iphoneNum withString:[NSString stringWithFormat:@"+%@ %@", _FPVC_areaNam, _FPVC_phoneNumber]];

    _newpassword.secureTextEntry = YES;
    [_newpassword addYTYanKTextFielStyle1:_newpassword withString:NSLocalizedString(@"再输入一次密码", nil)];

    _originalPassword.secureTextEntry = YES;
    [_originalPassword addYTYanKTextFielStyle1:_originalPassword withString:NSLocalizedString(@"请输入6-12位数的密码", nil)];

   
}


- (void)keyboardHide:(UIGestureRecognizer*)gestureRecognizer{
    [self.view endEditing:YES];
}


-(void)clickForgetPasswordBtn
{
    [self commitPwdClick:nil];
}


/**
 *  如下两个方法是用以处理输入内容是否符合规则
    弹出键盘的返回键next点击了会发生如何的反应效果
 *
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.originalPassword) {
        [self.originalPassword resignFirstResponder];
        [self.newpassword becomeFirstResponder];
    }
    
    if (textField == self.newpassword) {
        if (self.btnForgetPwd.enabled) {
            [self commitPwdClick:nil];
        }
    }
    return YES;
}

- (void)textLengthMethod:(UITextField*)textField{
    
    if (textField == self.originalPassword) {
        if (textField.text.length < 6 ) {
            self.btnForgetPwd.enabled = NO;
        }else{
            self.btnForgetPwd.enabled = YES;
        }
        if (textField.text.length > 12) {
            textField.text = [textField.text substringToIndex:12];
        }
    }
    if (textField == self.newpassword) {
        if (textField.text.length > 12) {
            textField.text = [textField.text substringToIndex:12];
        }
    }
}


#pragma mark 忘记密码，提交                                                                                        
- (IBAction)commitPwdClick:(id)sender {
    [self.view endEditing:YES];
    NSCharacterSet* whithNewChars = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString* newpassword = [_newpassword.text stringByTrimmingCharactersInSet:whithNewChars];
    NSString* password = [_originalPassword.text stringByTrimmingCharactersInSet:whithNewChars];
    if ([self judgeMentUserEnter:password againstPwd:newpassword]) {
        NSDictionary* param = @{ @"loginName" : _FPVC_phoneNumber,
                                 @"verifyCode" : _FPVC_SMSCode,
                                 @"zone" : _FPVC_areaNam,
                                 @"password" : password };
        [[UtilToolsClass getUtilTools] addDoLoading];
        __weak typeof (self)weakSelf = self;
        [NetRequestClass requestWithUrlForForgetPwd:self andUrl:[REQUESTHEADER stringByAppendingString:@"userInfo/userReset.asp"] andParam:param success:^(NSInteger result) {
            [[UtilToolsClass getUtilTools] removeDoLoading];
            if (result) {
                [ACUserLoginManage shareInstance].userPassword = password;
                [UD synchronize];
                [UtilToolsClass addDisapperAlert: @"" withMessage:NSLocalizedString(@"密码重置成功", nil)];
                [weakSelf.navigationController popToRootViewControllerAnimated:YES];
            }
            
        } failure:^(NSString *failure) {
            [[UtilToolsClass getUtilTools] removeDoLoading];
            [UtilToolsClass addDisapperAlert: @"" withMessage:failure];
        }];
    }
}


// 判断输入格式
- (BOOL) judgeMentUserEnter:(NSString *)newPwd againstPwd:(NSString *)agaPwd{
    
    BOOL result = true;
//    if (![PhoneNumber checkPasswordAndNum:newPwd]) {
//        [UtilToolsClass addDisapperAlert:@"" withMessage:NSLocalizedString(@"SETPASSWORD-ILLEGAL", nil)];
//        return false;
//    }
//     if (![PhoneNumber checkPasswordAndNum:agaPwd]) {
//        [UtilToolsClass addDisapperAlert:@"" withMessage:NSLocalizedString(@"SETPASSWORD-ILLEGAL", nil)];
//        return  false;
//    }
    if (![newPwd isEqualToString:agaPwd]) {
        [UtilToolsClass addDisapperAlert:@"" withMessage:NSLocalizedString(@"两次输入的密码不一致", nil)];
        return  false;
    }
    return result;
}





@end
