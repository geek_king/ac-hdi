//
//  RegisteredAccountVC.h
//  DataStatistics
//
//  Created by Kang on 16/1/4.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLLabel.h"
#import "MLLinkLabel.h"
#import "ACCustomButton.h"
@interface RegisteredAccountVC : UIViewController
/*! RegisteredAccountVC接收标识*/
@property (strong, nonatomic) NSString *RAVCsignStr;
/*! 主导语*/
@property (strong, nonatomic) IBOutlet UILabel *viceGuideLabel;
/*! 副导语 */
@property (strong, nonatomic) IBOutlet UILabel *guideLabel;
/*! 账号输入框 */
@property (strong, nonatomic) IBOutlet UITextField *iphoneNum;
/*! 条款 */
@property (weak, nonatomic) IBOutlet MLLinkLabel *Continue;

/*! 地区号 */
@property (strong, nonatomic) IBOutlet UILabel *areaCode;
/*! 地区名 */
@property (strong, nonatomic) IBOutlet UILabel *areaName;
/*! 地区按钮 */
@property (strong, nonatomic) IBOutlet ACCustomButton *clickAreaBtn;
/*! 地区数据 */
@property (nonatomic, strong) NSMutableArray *areaArray;
@property (strong, nonatomic) UIWindow *window;

@property (weak, nonatomic) IBOutlet UIButton *btnSMS;
/// 选择地区方法
- (IBAction)clickArea:(UIButton *)sender;
/// SMS方法
- (IBAction)clickSMSPinCodeBtn:(UIButton *)sender;
///- (IBAction)clickRegisteredAcountBtn:(UIButton *)sender;
@end