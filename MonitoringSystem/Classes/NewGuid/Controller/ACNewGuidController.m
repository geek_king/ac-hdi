//
//  ACNewGuidController.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 8/1/16.
//  Copyright © 2016 Interlube. All rights reserved.
//

#import "ACNewGuidController.h"
#import "ACNewGuidViewCell.h"

@interface ACNewGuidController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, weak) UIPageControl *control;
@end

@implementation ACNewGuidController
static NSString * const reuseIdentifier = @"Cell";

-(instancetype)init{
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc] init];
    //     设置尺寸
    layout.itemSize = [UIScreen mainScreen].bounds.size;
    //     设置间距
    layout.minimumLineSpacing = 0;
    //    设置方向
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    return [self initWithCollectionViewLayout:layout];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%s",__func__);
    // Register cell classes
    [self.collectionView registerClass:[ACNewGuidViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    //     设置翻页效果
    self.collectionView.pagingEnabled = YES;
    //      设置反弹效果
    self.collectionView.bounces = NO;
    //     设置隐藏滚动条
    self.collectionView.showsHorizontalScrollIndicator = NO;
}


#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ACNewGuidViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell setUpCollectionViewCellStyle:indexPath];
    [cell setIndexPath:indexPath and:3];
    return cell;
}
@end
