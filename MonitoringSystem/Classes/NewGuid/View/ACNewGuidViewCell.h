//
//  ACNewGuidViewCell.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 8/1/16.
//  Copyright © 2016 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACNewGuidViewCell : UICollectionViewCell

/*!
 *  设置翻页需要显示的页面
 *
 *  @param indexPath 当前层级，包含row，section
 */
-(void) setUpCollectionViewCellStyle:(NSIndexPath *)indexPath;

/*!
 *  用于判断是否显示开始按钮
 *
 *  @param indexPath
 *  @param count
 */
-(void) setIndexPath :(NSIndexPath *)indexPath and: (int) count;


@end
