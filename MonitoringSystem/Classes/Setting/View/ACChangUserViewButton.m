//
//  ACChangUserViewButton.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/26.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import "ACChangUserViewButton.h"

@implementation ACChangUserViewButton

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
         self.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    return self;
    
}

-(CGRect)imageRectForContentRect:(CGRect)contentRect{
    return CGRectMake(10,15, 17, 19);
}


- (CGRect) titleRectForContentRect:(CGRect)contentRect{
    CGFloat titleW = contentRect.size.width - 30;
    return CGRectMake(35,10, titleW, 30);
}


@end
