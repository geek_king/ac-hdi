//
//  ACChangeUserView.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/16.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import "ACChangeUserView.h"
#import "ACChangUserViewButton.h"
@interface ACChangeUserView ()<UIAlertViewDelegate>{
    CGFloat baseViewW; // 每个子控件的宽度
    CGFloat baseViewH;// 每个子控件的高度
}
@end

@implementation ACChangeUserView


- (instancetype)initWithFrame:(CGRect)frame withsubView:(NSInteger)objSub{
    if (self = [super initWithFrame:frame]) {
        // 背景
        UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        imgView.image = [UIImage imageNamed:@"xiala"];
        [self addSubview:imgView];
         baseViewW = self.bounds.size.width ;
         baseViewH = self.bounds.size.height / objSub;
        
    }
    return self;
}


- (void)initButtonSubViewWithImage:(UIImage *)img buttonTitle:(NSString *)title{
    CGFloat buttonViewY = baseViewH * (self.subviews.count - 1);
    ACChangUserViewButton * buttonView = [[ACChangUserViewButton alloc] initWithFrame:CGRectMake(0, buttonViewY, baseViewW, baseViewH)];
    [buttonView setImage:img forState:UIControlStateNormal];
    [buttonView setTitle:title forState:UIControlStateNormal];
    buttonView.tag = self.subviews.count - 1;
    [buttonView addTarget:self action:@selector(btnCallBack:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:buttonView];
}


- (void) btnCallBack:(UIButton *)sender{
    NSInteger tag = sender.tag;
    if (tag == 0) {
        // 修改密码
        if([self.delegate respondsToSelector:@selector(judgeToChangePwdOrLogOut:)]){
            [self.delegate judgeToChangePwdOrLogOut:false];
        }
    }else if (tag == 1){
        // 切换用户
        UIAlertView * alert =  [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示", nil) message:NSLocalizedString(@"您确定要退出当前登录?",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"取消",nil) otherButtonTitles:NSLocalizedString(@"确定",nil), nil];
        [alert show];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        // 修改密码
        if([self.delegate respondsToSelector:@selector(judgeToChangePwdOrLogOut:)]){
            [self.delegate judgeToChangePwdOrLogOut:true];
        }
    }
}

@end



