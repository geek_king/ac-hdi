//
//  ACChangeUserView.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/16.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChangeUserViewDelegate <NSObject>
/*!
 *
 *
 *  @param isLogout 是否登出操作
 */
- (void) judgeToChangePwdOrLogOut:(BOOL) isLogout;

@end

@interface ACChangeUserView : UIView


/*!
 *   delegate 是ChangeUserViewDelegate委托对象
 */
@property(nonatomic,assign) id<ChangeUserViewDelegate> delegate;


/*!
 *  在创建视图大小的时候，根据子视图的个数来决定其大小的位置
 *
 *  @param frame  父类控件的大小
 *  @param objSub 子类的数量
 *
 *  @return 返回当前的类的实例对象
 */
- (instancetype)initWithFrame:(CGRect)frame withsubView:(NSInteger)objSub;



/*!
 *  根据传入的按钮、文字来创建子控件。只开放给按钮
 *
 *  @param img   图片
 *  @param title 文本内容
 */
- (void) initButtonSubViewWithImage:(UIImage *)img buttonTitle:(NSString *)title;
@end
