//
//  ACUserChangePwdController.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/26.
//  Copyright © 2016年 YTYangK. All rights reserved.
//


#import "ACUserChangePwdController.h"
#import "NetRequestClass.h"
#import "UtilToolsClass.h"
#import "ACUserLoginManage.h"

@interface ACUserChangePwdController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userOldPwd;
@property (weak, nonatomic) IBOutlet UITextField *userNewPwd;
@property (weak, nonatomic) IBOutlet UITextField *userNewPwdAgainst;
@property (weak, nonatomic) IBOutlet UIButton *btnOKCallBack;
- (IBAction)commitInformationClick:(id)sender;

@end

@implementation ACUserChangePwdController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"修改密码", nil);
    self.userOldPwd.placeholder = NSLocalizedString(@"请输入原密码", nil);
    self.userNewPwd.placeholder = NSLocalizedString(@"请输入新的6-12位数的密码", nil);
    self.userNewPwdAgainst.placeholder = NSLocalizedString(@"确认密码", nil);
    [self.btnOKCallBack setTitle: NSLocalizedString(@"确定", nil) forState:UIControlStateNormal];
    
    // 设置返回按钮
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back@3x"] style:UIBarButtonItemStylePlain target:self action:@selector(backItemClick)];
    self.navigationItem.leftBarButtonItem = backItem;
    self.userOldPwd.delegate = self;
    self.userNewPwd.delegate = self;
    self.userNewPwdAgainst.delegate = self;
    [self.userOldPwd becomeFirstResponder];
    self.btnOKCallBack.enabled = NO;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_userOldPwd addTarget:self action:@selector(textLengthMethod:) forControlEvents:UIControlEventEditingChanged];
    [_userNewPwd addTarget:self action:@selector(textLengthMethod:) forControlEvents:UIControlEventEditingChanged];
    [_userNewPwdAgainst addTarget:self action:@selector(textLengthMethod:) forControlEvents:UIControlEventEditingChanged];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
}

- (void)textLengthMethod:(UITextField *)textField{
    if (_userNewPwd == textField || _userNewPwdAgainst == textField || _userOldPwd == textField) {
        if (_userNewPwdAgainst.text.length > 12 ||  _userNewPwd.text.length > 12 || _userOldPwd.text.length > 12 ) {
               textField.text = [textField.text substringToIndex:12];
            return;
        }
    }
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.userNewPwd) {
        
        if ( ![self.userOldPwd.text isEqualToString:[ACUserLoginManage shareInstance].userPassword]) {
            [UtilToolsClass addDisapperAlert:NSLocalizedString(@"提示",nil) withMessage:NSLocalizedString(@"密码错误,请重新输入", nil)];
            return NO;
        }else{
            self.btnOKCallBack.enabled = YES;
        }
    }
       return YES;
}

 -(BOOL)textFieldShouldReturn:(UITextField *)textField{
 
     if (textField == self.userOldPwd) {
        
         [self.userOldPwd resignFirstResponder];
         [self.userNewPwd becomeFirstResponder];
       }
     
     if (textField == self.userNewPwd) {
         [self.userNewPwd resignFirstResponder];
         [self.userNewPwdAgainst becomeFirstResponder];
     }
     if (textField == self.userNewPwdAgainst) {
         [self.view endEditing:YES];
         // 登陆
         [self commitInformationClick:nil];
     }
     return YES;
     
 }
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)backItemClick{
  
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 更改用户密码
- (IBAction)commitInformationClick:(id)sender {

    if ([self checkingUserEnter]) {
        [[UtilToolsClass getUtilTools] addDoLoading];
        [NetRequestClass requestWithUrlChangeUserPwd:self andUrl:[REQUESTHEADER stringByAppendingString:@"account/updatePwd.asp"] andParam:@{@"user_id":[ACUserLoginManage shareInstance].userCode,@"oldPwd":_userOldPwd.text,@"newPwd":_userNewPwd.text} success:^(NSString *codeStr) {
            [[UtilToolsClass getUtilTools] removeDoLoading];
            if([codeStr isEqualToString:@"修改成功"]){
                [ACUserLoginManage shareInstance].userPassword = _userNewPwd.text;
                [UD synchronize];
            }
            [UtilToolsClass addDisapperAlert:@"" withMessage:NSLocalizedString(@"修改成功",nil)];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
                ACApplicationKeyWindow.rootViewController =  [storyBoard instantiateInitialViewController];
            });
            
        } failure:^(NSString *error) {
            [[UtilToolsClass getUtilTools] removeDoLoading];
            [UtilToolsClass addDisapperAlert:@"" withMessage:error];
        }];
    }
}


// 检查用户输入
- (BOOL) checkingUserEnter{
    if(_userNewPwd.text.length == 0 || [_userNewPwd.text isEqualToString:@""] || [[_userNewPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]){
        [UtilToolsClass addDisapperAlert:NSLocalizedString(@"提示",nil) withMessage:NSLocalizedString(@"新密码不能为空",nil)];
        _userNewPwd.text = @"";
        return false;
    }
    if (_userNewPwd.text.length < 6 ) {
        
        [UtilToolsClass addDisapperAlert:NSLocalizedString(@"提示",nil) withMessage:NSLocalizedString(@"请输入新的6-12位数的密码",nil)];
        return false;
    }
    
    if (![_userNewPwd.text isEqualToString:_userNewPwdAgainst.text]) {
        [UtilToolsClass addDisapperAlert:NSLocalizedString(@"提示",nil) withMessage:NSLocalizedString(@"两次输入的密码不一致",nil)];
        return false;
    }
    return true;
}
@end
