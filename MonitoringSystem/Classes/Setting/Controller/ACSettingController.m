//
//  ACSettingController.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/15.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import "ACSettingController.h"
#import "MapGPSViewController.h"
#import "UtilToolsClass.h"
#import "ACWebviewViewController.h"
#import "ACCustomButton.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "ACChangeUserView.h"
#import "ACUserChangePwdController.h"
#import "UtilToolsClass.h"
#import "NetRequestClass.h"
#import "JPUSHService.h"
@interface ACSettingController ()<MFMailComposeViewControllerDelegate,ChangeUserViewDelegate>{
    BOOL isShow;
}
- (IBAction)showCompanyMapCallBack:(id)sender;
- (IBAction)showCompanyEmialCallBack:(id)sender;
- (IBAction)phoneCallBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *cCompanyName;
@property (weak, nonatomic) IBOutlet ACCustomButton *btnCompanyName;
@property (weak, nonatomic) IBOutlet ACCustomButton *btnCompanyEmial;
@property(nonatomic,strong) ACChangeUserView * changeUserView;
@property (weak, nonatomic) IBOutlet UIButton *btnCallPhone;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companyLogoConstraint;
@end

@implementation ACSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    isShow = false;
   
    if (SCREEN_HEIGHT == 568) {
        // 4.0
        self.btnCompanyName.titleLabel.font = [UIFont systemFontOfSize:14];
         self.btnCompanyEmial.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    else if (SCREEN_HEIGHT == 667) {
        //4.7
            self.companyLogoConstraint.constant = self.companyLogoConstraint.constant - 40;
    }else if ( SCREEN_HEIGHT == 736){
        // 5.5
        self.companyLogoConstraint.constant = self.companyLogoConstraint.constant - 50;
    }
    
    [self translateLanguage];
    
    // 设置返回按钮
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back@3x"] style:UIBarButtonItemStylePlain target:self action:@selector(backItemClick)];
    self.navigationItem.leftBarButtonItem = backItem;
    self.navigationItem.hidesBackButton = YES;
    
    UIBarButtonItem* rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-yonghuxiala"] style:UIBarButtonItemStylePlain target:self action:@selector(rightItemClick)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
}

- (void) translateLanguage{
     self.title = NSLocalizedString(@"关于我们", nil);
    self.cCompanyName.text = NSLocalizedString(@"英特路(中国)机械设备有限公司", nil);
    [self.btnCompanyName setTitle:NSLocalizedString(@"珠海市南屏科技园虹达路3号", nil) forState:UIControlStateNormal];
}

- (void)backItemClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightItemClick{
    
    if (isShow) {
        [self.changeUserView removeFromSuperview];
        isShow = false;
        
    }else{
        CGFloat changeUserViewW = 160;
        CGFloat changeUserViewY = 0;
        ACChangeUserView * changeUserView = [[ACChangeUserView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - changeUserViewW - 15, changeUserViewY, changeUserViewW, 80) withsubView:2];
        changeUserView.delegate = self;
        [changeUserView initButtonSubViewWithImage:[UIImage imageNamed:@"icon-xiugaimima"] buttonTitle:NSLocalizedString(@"修改密码",nil)];
        [changeUserView initButtonSubViewWithImage:[UIImage imageNamed:@"icon-qiehuanyonghu"] buttonTitle:NSLocalizedString(@"切换用户",nil)];
        [self.view addSubview:changeUserView];
        [self.view bringSubviewToFront:changeUserView];
        self.changeUserView = changeUserView;
        isShow = true;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
     isShow = false;
     [self.changeUserView removeFromSuperview];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.changeUserView removeFromSuperview];
    isShow = false;
    
}


- (void) judgeToChangePwdOrLogOut:(BOOL) isLogout{
     [self.changeUserView removeFromSuperview];
    if (isLogout) {
        [self userLogOutClick];
    }else{
        
        UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ACUserChangePwdController * changePwdController =   [storyBoard instantiateViewControllerWithIdentifier:@"ChangeUserPasswordID"];
        [self.navigationController pushViewController:changePwdController animated:YES];
    }

}


#pragma mark 退出操作
- (void)userLogOutClick {
    
    [[UtilToolsClass getUtilTools] addDoLoading];
    __weak typeof(self) weakSelf = self;
    [NetRequestClass requestWithUrlForLogOut:self andUrl:[REQUESTHEADER stringByAppendingString:@"login/logout.asp"] andParam:nil success:^(BOOL result) {
        [[UtilToolsClass getUtilTools] removeDoLoading];
        if (result) {
            [weakSelf unBindAccountForPush];
            // 移除本地的session
            [UD setObject:nil forKey:@"ac-userCookie"];
            [ACUserLoginManage shareInstance].userPassword = @"";
            UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
            [[[UIApplication sharedApplication] delegate] window].rootViewController =  [storyBoard instantiateInitialViewController];
        }else{
            [UtilToolsClass addDisapperAlert:@"" withMessage:@"Logout failure"];
        }
    } failure:^(NSString *failure) {
        [[UtilToolsClass getUtilTools] removeDoLoading];
        [UtilToolsClass addDisapperAlert:@"" withMessage:failure];
    }];
    
    
}

#pragma mark 解除绑定推送操作
- (void) unBindAccountForPush{
    // 设置推送别名
    [JPUSHService setAlias:@"" callbackSelector:@selector(tagsAliasCallback:tags:alias:) object:self];
}


- (void)tagsAliasCallback:(int)iResCode tags:(NSSet*)tags alias:(NSString*)alias {
    NSLog(@"rescode: %d, \ntags: %@, \nalias: %@\n", iResCode, tags , alias);
    NSLog(@"此处进行解绑设备");
}




#pragma mark 展示地图
- (IBAction)showCompanyMapCallBack:(id)sender {
    MapGPSViewController * mapController = [[MapGPSViewController alloc] init];
    mapController.isEnterSetting = true;
    [self.navigationController pushViewController:mapController animated:YES];
}



#pragma mark 发送邮件
- (IBAction)showCompanyEmialCallBack:(id)sender {

    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (!mailClass) {
        [UtilToolsClass addDisapperAlert:NSLocalizedString(@"提示",nil) withMessage:@"当前系统版本不支持应用内发送邮件功能"];
        return;
    }
    if (![mailClass canSendMail]) {
        [UtilToolsClass addDisapperAlert:NSLocalizedString(@"提示",nil) withMessage:NSLocalizedString(@"用户没有设置邮件账户",nil)];
        return;
    }
    [self displayMailPicker:sender];

}

//调出邮件发送窗口
- (void)displayMailPicker:(UIButton *)sender
{
    MFMailComposeViewController *mailPicker = [[MFMailComposeViewController alloc] init];
    mailPicker.mailComposeDelegate = self;
    
    //设置主题
    [mailPicker setSubject: @"eMail"];
    //添加收件人
    NSArray *toRecipients = [NSArray arrayWithObject: sender.currentTitle];
    [mailPicker setToRecipients: toRecipients];
    NSString *emailBody = @"<font color='red'>eMail</font>";
    [mailPicker setMessageBody:emailBody isHTML:YES];
    [self presentViewController:mailPicker animated:YES completion:nil];
   
}


#pragma mark - 实现 MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    //关闭邮件发送窗口
    [self dismissViewControllerAnimated:YES completion:nil];
    NSString *msg;
    switch (result) {
        case MFMailComposeResultCancelled:
            msg = NSLocalizedString(@"邮件取消发送",nil);
            break;
        case MFMailComposeResultSaved:
            msg = NSLocalizedString(@"邮件发送失败,保存到草稿箱中",nil);
            break;
        case MFMailComposeResultSent:
            msg = NSLocalizedString(@"邮件发送成功",nil);
            break;
        case MFMailComposeResultFailed:
            msg = NSLocalizedString(@"邮件发送失败",nil);
            break;
        default:
            msg = @"";
            break;
    }
   [UtilToolsClass addDisapperAlert:@"" withMessage:msg];
}


- (IBAction)phoneCallBack:(id)sender {

    UIWebView * callWebview = [[UIWebView alloc] init];
    [callWebview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"tel:400-688-9838"]]];
    [self.view addSubview:callWebview];
}
@end
