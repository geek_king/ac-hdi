//
//  ACQRResultController.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/9.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

/**
 *      二维码扫描的结果
 */
#import <UIKit/UIKit.h>
@class ACQRDeviceModel;
@class ACDeviceDetailModel;
@class ACQRModelBase;
@interface ACQRResultController : UIViewController

- (instancetype)initWithModel:(ACQRModelBase * )qrmodel;
/*!
 *  二维码扫描识别到的内容
 */
@property (nonatomic, strong) ACQRDeviceModel* QRModel;

/*!
 *    从二维码扫描后进去此界面
 */
@property(nonatomic,assign) BOOL enterWithQR;

@property(nonatomic,strong) NSString * i_imei;

@end
