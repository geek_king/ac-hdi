//
//  ACChangeDeviceNameController.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/4.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import "ACChangeDeviceNameController.h"
#import "NSString+EmojiExtension.h"
@interface ACChangeDeviceNameController ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textName;
@end


@implementation ACChangeDeviceNameController
- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.title = NSLocalizedString(@"修改昵称", nil);
    if ([self.oldName isEqualToString:@"点击添加设备昵称"]) {
        self.textName.text = @"";
    }else{
        self.textName.text = self.oldName;
    }
    
    UIButton * defaultClearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    defaultClearButton.frame =CGRectMake(SCREEN_WIDTH - 40 ,CGRectGetMaxY(self.textName.frame) - 20 ,20,20);
    [defaultClearButton setImage:[UIImage imageNamed:@"TextfiledClose"] forState:UIControlStateNormal];
    [defaultClearButton addTarget:self action:@selector(clearButtonSelected) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:defaultClearButton];
    [self.textName becomeFirstResponder];
    /**
     *  //此处使底部线条颜色为红色
     */
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    [navigationBar setBackgroundImage:[[UIImage alloc] init] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [navigationBar setShadowImage:[UIImage createImageWithColor:[UIColor colorWithRed:108/255.0 green:112/255.0 blue:112/255.0 alpha:1]]];
    
    // 设置返回按钮
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back@3x"] style:UIBarButtonItemStylePlain target:self action:@selector(backItemClick)];
    self.navigationItem.leftBarButtonItem = backItem;
    
    // 设置右边的按钮
    UIBarButtonItem* trueItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"OK"] style:UIBarButtonItemStylePlain target:self action:@selector(trueItemClick)];
    self.navigationItem.rightBarButtonItem = trueItem;
    self.navigationItem.hidesBackButton = YES;
      self.textName.delegate = self;
}

-(void)clearButtonSelected{
    self.textName.text = @"";
}

-(void)backItemClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)returnText:(PushDeviceName)block{
    self.pushDeviceName = block;
}

- (void)trueItemClick{
    // block回调传值
    if (self.pushDeviceName) {
           self.pushDeviceName([self.textName.text removeEmoji]);
    }
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)textViewDidChange:(UITextView *)textView{
    if (textView == self.textName) {
        if (textView.text.length >= 8) {
            textView.text = [textView.text substringToIndex:8];
        }
    }
}


// 禁止换行
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    if ( [text isEqualToString:@"\n"] ) {
        // block回调传值
        if (self.pushDeviceName) {
            self.pushDeviceName([self.textName.text removeEmoji]);
        }
        [self.navigationController popViewControllerAnimated:YES];
        return  false;
    }
    return YES;
}


@end
