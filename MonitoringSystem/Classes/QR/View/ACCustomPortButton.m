//
//  ACCusPortButton.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 9/3/16.
//  Copyright © 2016 Interlube. All rights reserved.
//
/**
 *  此自定义类，服务于英文显示，12点 === 12 point 在英文情况下，系统的button不能够被使用
 */
#import "ACCustomPortButton.h"

@implementation ACCustomPortButton

-(CGRect)imageRectForContentRect:(CGRect)contentRect{
    return CGRectMake(0,0, 20, 20);
   
}

- (CGRect) titleRectForContentRect:(CGRect)contentRect{
    CGFloat W = contentRect.size.width - 5;
    return CGRectMake(22,0, W, 20);
}

@end
