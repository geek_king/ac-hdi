//
//  ACDeviceDetailModel.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/11.
//  Copyright © 2016年 Interlube. All rights reserved.
//
#import "ACQRModelBase.h"

@interface ACDeviceDetailModel : ACQRModelBase

/*!
 *  imei码
 */
@property(nonatomic,strong) NSString * code;
/*!
 *  设备类型
 */
@property(nonatomic,strong) NSString * type;
/*!
 *  端口数
 */
@property(nonatomic,strong) NSString * number;
/*!
 *  昵称
 */
@property(nonatomic,strong) NSString * nickName;
/*!
 *  手机号码
 */
@property(nonatomic,strong) NSString * card;

/*!
 *  设备拥有者的userID
 */
@property(nonatomic,strong) NSString * employer;


@property(nonatomic,strong) NSString * zone;

/*!
 *  当前登陆对象是否是该设备的管理者
 */
@property(nonatomic,assign) BOOL isEmployer;

/*!
 *  图片路径
 */
@property(nonatomic,strong) NSMutableArray * img_url;

- (instancetype) initWithDic:(NSDictionary *)dic;

+ (instancetype) deviceDetailModelWithDic:(NSDictionary *)dic;
@end
