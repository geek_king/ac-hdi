//
//  ACQRDeviceModel.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/13.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACQRModelBase.h"

@interface ACQRDeviceModel : ACQRModelBase


/*!
 *  设备类型
 */
@property (nonatomic,strong) NSString * type;

/*!
 *  3d卡号
 */
@property (nonatomic,strong) NSString * card;


/*!
 *  芯片序列号
 */
@property (nonatomic,strong) NSString * imei;

/*!
 *  设备端口数
 */
@property(nonatomic,assign) NSInteger number;

/*!
 *  设备拥有者的userID
 */
@property(nonatomic,strong) NSString * employer;

/*!
 *  管理员号码的区号
 */
@property(nonatomic,strong) NSString * zone;

/*!
 *  昵称
 */
@property(nonatomic,strong) NSString * nickName;

/*!
 *  图片路径
 */
@property(nonatomic,strong) NSMutableArray * img_url;
/*!
 *  当前登陆对象是否是该设备的管理者
 */
@property(nonatomic,assign) BOOL isEmployer;


-(instancetype) initWithDic:(NSDictionary *)dic;

+ (instancetype) qrDeviceWithDic:(NSDictionary *)dic;


@end
