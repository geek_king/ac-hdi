//
//  ACDeviceViewCell.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/1.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ACDeviceListModel;

@protocol ACDeviceViewCellDelegate <NSObject>

- (void) cellDelegateCallBackWith:(NSInteger) enterTag imei:(NSString *)imei;
@end


@interface ACDeviceViewCell : UITableViewCell

/*!
 *  设备模型
 */
@property (nonatomic,strong)  ACDeviceListModel* listModel;

/*!
 *   委托回调
 */
@property (nonatomic,assign) id<ACDeviceViewCellDelegate> delegate;

@end

