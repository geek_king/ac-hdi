//
//  ACDeviceViewCell.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/1.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACDeviceViewCell.h"
#import "ACDeviceListModel.h"
#import "AddressView.h"
#import "ACQRResultController.h"
#import "UIImage+Extension.h"
#import "ACOilButton.h"
#import "UtilToolsClass.h"
#import <UIImageView+WebCache.h>
@interface ACDeviceViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *deviceName; // 设备标号
@property (weak, nonatomic) IBOutlet UILabel *lblTime;   // 时间
@property (weak, nonatomic) IBOutlet ACOilButton *btnEoil;// 剩余油量值
@property (weak, nonatomic) IBOutlet UIImageView *devicePic;// 设备图片
@property (weak, nonatomic) IBOutlet UIImageView *deviceWarning;
@property(nonatomic,strong) ACDeviceListModel *deviceListodel;
@property (weak, nonatomic) IBOutlet UIButton *shfitBtn;

@property (weak, nonatomic) IBOutlet UIButton *enterDeviceInfoCallBack; // 点击头像进入
@property (weak, nonatomic) IBOutlet UIButton *deviceAddress; // 地址
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gpsAddressConstraint;
@end

@implementation ACDeviceViewCell



- (IBAction)enterDeviceGPS:(UIButton *)sender {
    sender.tag = 1;
    [self deviceInfoCallBack:sender];
}

-(void)awakeFromNib{

    [super awakeFromNib];
 
    self.deviceAddress.titleLabel.lineBreakMode = 0;
    self.deviceAddress.titleLabel.numberOfLines = 0;
    
    self.enterDeviceInfoCallBack.tag = 0;
    self.deviceAddress.tag = 1;
    self.btnEoil.userInteractionEnabled = NO;
    [self.enterDeviceInfoCallBack addTarget:self action:@selector(deviceInfoCallBack:) forControlEvents:UIControlEventTouchUpInside];
    self.deviceAddress.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 10, 120);
     self.deviceAddress.titleEdgeInsets = UIEdgeInsetsMake(5, 5, 0, 0);
    
    if (SCREEN_HEIGHT == 568) {
        //4.0
    }else if (SCREEN_HEIGHT == 667){
        // 4.7
         self.gpsAddressConstraint.constant = self.gpsAddressConstraint.constant + 20;
    }else if (SCREEN_HEIGHT == 736){
        // 5.5
        self.gpsAddressConstraint.constant = self.gpsAddressConstraint.constant + 50;
    }
    
}

- (void) deviceInfoCallBack:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(cellDelegateCallBackWith:imei:)]) {
        [self.delegate cellDelegateCallBackWith:sender.tag imei:self.enterDeviceInfoCallBack.accessibilityIdentifier];
    }
}


- (UIViewController *)getCurrentVC{
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UINavigationController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}



- (void)setListModel:(ACDeviceListModel *)listModel{
    self.deviceListodel = listModel;
    self.enterDeviceInfoCallBack.accessibilityIdentifier = [NSString stringWithFormat:@"%@",listModel.code];
    // 昵称
    NSString * showNickName = @"";
    if ([listModel.d_nickName isEqualToString:@""]) {
        showNickName = listModel.code;
    }else{
        showNickName = listModel.d_nickName;
    }
    self.deviceName.text = showNickName;
    // 时间
    self.lblTime.text = listModel.end_time;
    // 档位
    [self.shfitBtn setTitle:[NSString stringWithFormat:@"%d",(listModel.shift - 1)] forState:UIControlStateNormal];
    // 右侧显示区域
    if(listModel.i_state == 1 || listModel.i_state == 2){
        self.deviceWarning.hidden = YES;
        self.btnEoil.hidden = NO;
        // 油量  油量的背景------  cell会重用，需要移除后，重新生成------
        [self.btnEoil removeOilBG];
        [self.btnEoil setOilBGImage:[self createImageForEndOil:listModel.oilIcon endOild:listModel.end_oil]];
        // 设置油量
        [self.btnEoil setTitle:[NSString stringWithFormat:@"%.f%%",listModel.end_oil] forState:UIControlStateNormal];
        
    }else if(listModel.i_state == 4){
       // 非作业
        self.deviceWarning.image = [UIImage imageNamed:@"icon-fengyunzuo"];
        self.deviceWarning.hidden = NO;
        self.btnEoil.hidden = YES;

    }else if ( listModel.i_state == 3){
        // 故障
         self.deviceWarning.image = [UIImage imageNamed:@"icon-gaojing@3x"];
        self.deviceWarning.hidden = NO;
        self.btnEoil.hidden = YES;

    }else{}
    // 地址
    if (listModel.lon !=0  && listModel.lon != 0) {
        [AddressView getAddressByLatitude:listModel.lat longitude:listModel.lon addrees:^(NSDictionary *placemark) {
            NSString * city =  @"";
            if ( [UtilToolsClass judgeLocalLanguage] == 1) {
                // 中文
               city = [NSString stringWithFormat:@"%@",placemark[@"City"]];
                city = [city stringByAppendingFormat:@"%@",placemark[@"SubLocality"]];
            }else{
               city = [NSString stringWithFormat:@"%@",placemark[@"SubLocality"]];
                city = [city stringByAppendingFormat:@",%@",placemark[@"City"]];
            }
            [self.deviceAddress setTitle:city forState:UIControlStateNormal];
            NSLog(@"placemark :%@",placemark);
        }];
        
    }else{
       [self.deviceAddress setTitle:@"---" forState:UIControlStateNormal];
    }
    self.deviceAddress.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    self.deviceAddress.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, self.deviceAddress.width - 20);
    // 头像
    if (![listModel.img isEqualToString:@""]) {
        self.devicePic.layer.cornerRadius = 10;
        self.devicePic.layer.masksToBounds = YES;
    }
    NSLog(@"头像 :%@",[USER_DEVICE_IMAGE_URL stringByAppendingFormat:@"%@",listModel.img]);
    [self.devicePic sd_setImageWithURL:[NSURL URLWithString:[USER_DEVICE_IMAGE_URL stringByAppendingFormat:@"%@",listModel.img]] placeholderImage:[UIImage imageNamed:@"icon-list-default"] ];
}


/**
 *  绘制uiiameg 根据油量的剩余，自动生成uiimage。
 */
- (UIImage *) createImageForEndOil:(OILICON)oil  endOild:(CGFloat ) endOil{

    UIColor * tempColor = [UIColor clearColor];
    if (oil == SURPLUSENOUGH) {
        // 绿色充足
        tempColor = [UIColor colorWithRed:4 / 255.0 green:153 /255.0 blue:0.0 alpha:1];
    }else if (oil == SURPLUSGENERAL){
        // 黄色中等
        tempColor = [UIColor colorWithRed:245 / 255.0 green:179 /255.0 blue:22 / 255.0 alpha:1];
    }else if (oil == SURPLUSDRIES){
        // 红色告急
        tempColor = [UIColor redColor];
    }else if(oil == SURPLUSWARNING){
        tempColor  = [UIColor clearColor];
    }else{
        tempColor  = [UIColor clearColor];
    }
    CGFloat btnOilW = (self.btnEoil.bounds.size.width * endOil) / 100.0;
    
    return  [UIImage createImageWithColor:tempColor rect:CGRectMake(0, 0, btnOilW,self.btnEoil.bounds.size.height)];
}

- (UIColor *) fillTextColorForOilState:(OILICON)oil{
    UIColor * tempColor;
    if (oil == SURPLUSENOUGH) {
        // 绿色充足
        tempColor = [UIColor colorWithRed:4 / 255.0 green:153 /255.0 blue:0.0 alpha:1];
    }else if (oil == SURPLUSGENERAL){
        // 黄色中等
        tempColor = [UIColor colorWithRed:245 / 255.0 green:179 /255.0 blue:22 / 255.0 alpha:1];
    }else if (oil == SURPLUSDRIES){
        // 红色告急
        tempColor = [UIColor redColor];
    }else {
        tempColor = [UIColor redColor];
    }
    
    return  tempColor;
}

@end
