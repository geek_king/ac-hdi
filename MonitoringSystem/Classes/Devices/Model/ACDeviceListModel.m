//
//  ACDeviceListModel.m
//  MonitoringSystem
//  Created by JiaKang.Zhong on 16/5/17.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
#import "ACDeviceListModel.h"
#import "UtilToolsClass.h"

@implementation ACDeviceListModel

- (instancetype)initWithDic:(NSDictionary *)dic{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
        int tempOil =  [dic[@"end_oil"] intValue];
        if(tempOil >= 50){
            // 绿色
            _oilIcon = SURPLUSENOUGH;
        }else if(tempOil >= 20){
            // 黄色
            _oilIcon = SURPLUSGENERAL;
        }else if (tempOil > 0){
            // 红色
            _oilIcon = SURPLUSDRIES;
        }else{
            // 警告或者是非作业
            _oilIcon = SURPLUSWARNING;
        }
        // 时间格式化
        _end_time = [UtilToolsClass timestampStringTransformDateString:[dic[@"end_time"] longLongValue]];
    }
    return self;
}


+ (instancetype)listModelWithDic:(NSDictionary *)dic{
    return [[self alloc] initWithDic:dic];
}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
    
}
@end
