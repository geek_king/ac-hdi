//
//  ACDeviceListModel.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/17.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>
/*!
 *  油量的枚举类型
 */
typedef enum{
    SURPLUSENOUGH, // 绿色充足
    SURPLUSGENERAL, // 黄色中等
    SURPLUSDRIES, // 红色告急
    SURPLUSWARNING, // 警告

}OILICON;
@interface ACDeviceListModel : NSObject

/*!
 *  设备编号
 */
@property (nonatomic,strong) NSString * code;

/*!
 *  设备昵称
 */
@property(nonatomic,strong) NSString * d_nickName;

/*!
 *  时间
 */
@property (nonatomic,strong) NSString *  end_time;

/*!
 *  地址
 */
@property (nonatomic,strong) NSString * address;

/*!
 *  工作时间
 */
@property(nonatomic,assign) int setTime;
/*!
 *  经度
 */
@property(nonatomic,assign) double lon;

/*!
 *  纬度
 */
@property(nonatomic,assign) double lat;

/*!
 *  档位
 */
@property(nonatomic,assign) int shift;

/*!
 *  油量的剩余天数
 */
@property (nonatomic,assign) CGFloat end_day;

/*!
 *  油量对应的图标 根据剩余油量演变对应的图标
 */
@property (nonatomic,assign) OILICON oilIcon;

/*!
 *  油量剩余数
 */
@property (nonatomic,assign) CGFloat  end_oil;

/*!
 *  设备图片
 */
@property(nonatomic,strong) NSString * img;

/*!
 *  设备状态
 */
@property(nonatomic,assign) int i_state;

/*!
 *  绑定的数量
 */
@property(nonatomic,assign) int bindings;

/*!
 *  当前用户是不是管理员
 */
@property(nonatomic,assign) BOOL isEmployer;

/*!
 *  实例对象内容构造器
 *
 *  @param dic 传入的字典数据流
 *
 *  @return 返回实例对象
 */
- (instancetype) initWithDic:(NSDictionary *)dic;

/*!
 *  外部对象的构造器
 *
 *  @param dic 传入的字典数据流
 *
 *  @return 返回实例对象
 */
+ (instancetype) listModelWithDic:(NSDictionary *)dic;

@end
