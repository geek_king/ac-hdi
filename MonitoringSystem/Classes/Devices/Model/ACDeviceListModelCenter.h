//
//  ACDeviceListModelCenter.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/17.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACDataEntity.h"

@interface ACDeviceListModelCenter : ACDataEntity


/*!
 *  所有的设备数量
 */
@property(nonatomic,assign) int allDeviceCount;

/*!
 *  正常的设备数量
 */
@property(nonatomic,assign) int normalDeviceCount;

/*!
 *  故障的设备数量
 */
@property(nonatomic,assign) int breakDownDeviceCount;
/*!
 *  所有的设备
 */
@property (nonatomic,strong) NSMutableArray * allDevice;

/*!
 *  正常的设备
 */
@property(nonatomic,strong) NSMutableArray * normarlDevice;

/*!
 *  非作业的设备
 */
@property(nonatomic,strong) NSMutableArray * noWorkDevice;

/*!
 *  马达报警设备
 */
@property(nonatomic,strong) NSMutableArray * breakDownDevice;


@property (nonatomic,strong) NSMutableArray * breakDownAndNoWorkDevice;

- (instancetype) initWithDic:(NSDictionary *)dic;

+ (instancetype) deveiceModelWithDic:(NSDictionary *)dic;

/*!
 *  在后台接受到数据后，转换为分类好的设备数组
 *
 *  @param arrDic    输入的数据字典流
 *  @param state     当前的用户点击的状态
 *  @param isReFresh 是否需要刷新
 */
- (void) converModelDataWithArray:(NSDictionary *) arrDic withbtnState:(NSInteger)state withIsReFresh:(BOOL ) isReFresh;

@end
