
//
//  CustomProgress.m
//  WisdomPioneer
//
//  Created by 主用户 on 16/4/11.
//  Copyright © 2016年 江萧. All rights reserved.
//

#import "CustomProgress.h"
@interface CustomProgress ()

@property(nonatomic,strong) UIImage * tGuidImage;
/**
 *  头部引导图
 */
@property(nonatomic,weak) UIImageView * guidImgView;
@end

@implementation CustomProgress


- (UIImageView *)guidImgView{
    if (!_guidImgView) {
            }
    return _guidImgView;
}

- (UIImageView *)bgimgView{
    if (!_bgimgView) {
        UIImageView * bgimgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        bgimgView.layer.borderColor = [UIColor clearColor].CGColor;
        bgimgView.layer.borderWidth =  1;
       [self addSubview:bgimgView];
        self.bgimgView = bgimgView;
    }
    return _bgimgView;
}



- (UIImageView *)leftimgView{
    if (!_leftimgView) {
        UIImageView * leftimgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,0, self.frame.size.height)];
        leftimgView.layer.borderColor = [UIColor clearColor].CGColor;
        leftimgView.layer.borderWidth =  1;
        [self addSubview:leftimgView];
        self.leftimgView = leftimgView;
    }
    return _leftimgView;
}


- (void)initProgressWithFrame:(CGRect) viewFrame BGImage:(UIImage *)bgImage leftImage:(UIImage *)leftImage guidImage:(UIImage *)guidImage maxFloat:(CGFloat)maxValue{
    self.frame = viewFrame;
    self.backgroundColor = [UIColor clearColor];
    self.bgimgView.image = bgImage;
    self.leftimgView.image = leftImage;
    self.tGuidImage = guidImage;
    self.maxValue = maxValue;
}


- (void) showGuidImageViewAnimation{
    if (!self.guidImgView.isAnimating) {
        UIImageView * t_GuidView = [[UIImageView alloc] initWithImage:self.tGuidImage];
        [t_GuidView sizeToFit];
        t_GuidView.center = CGPointMake(0,5);
        [self addSubview:t_GuidView];
        self.guidImgView = t_GuidView;
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionRepeat animations:^{
            self.guidImgView.alpha = 0.7;
            self.guidImgView.transform = CGAffineTransformMakeScale(0.7, 0.7);
        } completion:^(BOOL finished) {
            self.guidImgView.alpha = 1;
            self.guidImgView.transform = CGAffineTransformMakeScale(1, 1);
        }];
    }
}


-(void) stopGuidImageViewAnimtion{
    if (self.guidImgView.isAnimating) {
        [self.guidImgView stopAnimating];
    }
    [self.guidImgView removeFromSuperview];
    self.guidImgView = nil;
}

- (void) moveGuidViewX:(CGFloat) x y:(CGFloat )y{
    self.guidImgView.transform = CGAffineTransformMakeTranslation(x, y);
}

-(void)setPresent:(int)present{
    self.leftimgView.frame = CGRectMake(0, 0, self.frame.size.width/self.maxValue * present, self.frame.size.height);
    
}

@end
