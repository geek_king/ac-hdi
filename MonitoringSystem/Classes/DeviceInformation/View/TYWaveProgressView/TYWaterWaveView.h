//
//  TYWaterWaveView.h
//  TYWaveProgressDemo
//
//  Created by tanyang on 15/4/14.
//  Copyright (c) 2015 tanyang. All rights reserved.
//  核心波浪view

#import <UIKit/UIKit.h>

@interface TYWaterWaveView : UIView

/*!
 *   第一个波浪颜色
 */
@property (nonatomic, strong)   UIColor *firstWaveColor;

/*!
 *   第二个波浪颜色
 */
@property (nonatomic, strong)   UIColor *secondWaveColor;
/*!
 *   百分比
 */
@property (nonatomic, assign)   CGFloat percent;

/*!
 *  油量开始上升
 */
-(void) startWave;

/*!
 *  油量停止上升
 */
-(void) stopWave;

/*!
 *  重置
 */
-(void) reset;

@end
