//
//  CustomProgress.h
//  WisdomPioneer
//
//  Created by 主用户 on 16/4/11.
//  Copyright © 2016年 江萧. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomProgress : UIView
/*!
 *  背景
 */
@property(nonatomic, retain)UIImageView *bgimgView;
/*!
 *  进度图
 */
@property(nonatomic, retain)UIImageView *leftimgView;

/*!
 *  进度条最大值
 */
@property(nonatomic)float maxValue;

/*!
 *  设置进度
 */
-(void)setPresent:(int)present;
/*!
 *  显示并开始动画
 */
- (void) showGuidImageViewAnimation;

- (void) moveGuidViewX:(CGFloat) x y:(CGFloat )y;
/*!
 *  隐藏并停止动画
 */
-(void) stopGuidImageViewAnimtion;

/*!
 *  快捷初始化入口
 *
 */
- (void) initProgressWithFrame:(CGRect) viewFrame BGImage:(UIImage *)bgImage leftImage:(UIImage *)leftImage guidImage:(UIImage *)guidImage maxFloat:(CGFloat)maxValue ;
@end
