//
//  ACDeviceInfoDevice.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/22.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import "ACDeviceInfoModel.h"

@implementation ACDeviceInfoModel


+ (instancetype)deviceInfoWithDic:(NSDictionary *)dic{
    return [[self alloc] initWithDic:dic];
}


- (instancetype)initWithDic:(NSDictionary *)dic{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
         _isEmployer =  [dic[@"isEmployer"] intValue];// bool类型转整形
    }
    return self;
}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}



@end
