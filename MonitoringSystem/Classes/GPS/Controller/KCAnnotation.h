//
//  KCAnnotation.h
//  DataStatistics
//
//  Created by oilklenze on 15/11/3.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface KCAnnotation :MKPointAnnotation;

/*!
 *  图标
 */
@property (nonatomic,strong) UIImage  *icon;
/*!
 *  设备类型
 */
@property (nonatomic,strong) NSString *deviceType;
/*!
 *  上传的时间
 */
@property (nonatomic,copy  ) NSString *upload_time;
/*!
 *  设备昵称
 */
@property(nonatomic,strong) NSString* name;
@property (nonatomic,      ) int      index;
@property (nonatomic,strong) NSNumber * sort;
@end