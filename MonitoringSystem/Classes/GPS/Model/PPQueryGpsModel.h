//
//  PPQueryGpsModel.h
//  DataStatistics
//
//  Created by oilklenze on 15/12/8.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "JSONModel.h"

@interface PPQueryGpsModel : JSONModel
/**
 *  设备昵称或者IMEI
 */
@property(nonatomic,strong) NSString * d_name;

/**
 *  设备PK1
 */
@property(nonatomic,strong) NSString * pk1;
/**
 *  设备类型
 */
@property (nonatomic,strong) NSString  *d_type;
/**
 *  经度
 */
@property (nonatomic ) float     i_longitude_e;
/**
 *  纬度
 */
@property (nonatomic) float     i_latitude_n;
/**
 *  上传时间
 */
@property (nonatomic,strong) NSString  *i_recvTime;
/**
 *  定位类型
 */
@property (nonatomic       ) int       gtype;
/**
 *  运行状态
 */
@property (nonatomic       ) int       running_state;


- (instancetype) initWithDic:(NSDictionary *)dic;

+ (instancetype)queryGpsModelWithDic:(NSDictionary *)dic;


@end