//
//  PPQueryGpsModel.m
//  DataStatistics
//
//  Created by oilklenze on 15/12/8.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "PPQueryGpsModel.h"
#import "UtilToolsClass.h"
@implementation PPQueryGpsModel

- (instancetype)initWithDic:(NSDictionary *)dic{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
        self.i_recvTime = [UtilToolsClass timestampStringTransformDateString:[dic[@"i_recvTime"] longLongValue]];
    }
    return self;
}

+ (instancetype)queryGpsModelWithDic:(NSDictionary *)dic{
    
    return [[self alloc ] initWithDic:dic];
}


- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}

@end