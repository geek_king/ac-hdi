//
//  ACRecommedHeadView.h
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/11.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACRecommedHeadView : UIView


/**
 *  文字  图片
 */
- (instancetype)initWithTitle:(NSString *) title withImage:(UIImage *)imgView;
@end
