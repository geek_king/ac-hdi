//
//  ACRecommedViewCell.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 16/7/11.
//  Copyright © 2016年 Interlube. All rights reserved.
//

#import "ACRecommedViewCell.h"

@interface ACRecommedViewCell ()

@property (weak, nonatomic) IBOutlet UIButton *p1CircleBtn;
@property (weak, nonatomic) IBOutlet UIButton *p2CircleBtn;
@property (weak, nonatomic) IBOutlet UIButton *p3CircleBtn;
@property (weak, nonatomic) IBOutlet UIButton *p4CircleBtn;
@property (weak, nonatomic) IBOutlet UIButton *p5CircleBtn;
@property (weak, nonatomic) IBOutlet UIButton *p6CircleBtn;
@property (weak, nonatomic) IBOutlet UIButton *p7CircleBtn;

@property(nonatomic,strong) NSMutableArray * portArr;
@end

@implementation ACRecommedViewCell


- (NSMutableArray *)portArr{
    if(!_portArr){
        _portArr = [NSMutableArray array];
    }
    return _portArr;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    NSLog(@"%s",__func__);
    
    [self.portArr addObject:_p1CircleBtn];
    [self.portArr addObject:_p2CircleBtn];
    [self.portArr addObject:_p3CircleBtn];
    [self.portArr addObject:_p4CircleBtn];
    [self.portArr addObject:_p5CircleBtn];
    [self.portArr addObject:_p6CircleBtn];
    [self.portArr addObject:_p7CircleBtn];
}




- (void)setConfigDic:(NSDictionary *)configDic{
    
    // key是颜色标记 ----------  value 是对应的值
    int index = 0;
    for (NSString *valueKeys in configDic.allKeys) {
        if (![valueKeys isEqualToString:@"configPk"]) {
            UIButton * tempBtn = (UIButton *)self.portArr[index];
            int values = [[configDic objectForKey:valueKeys] intValue];
            if (values) {
                [tempBtn setTitle:[NSString stringWithFormat:@"%d",values] forState:UIControlStateNormal];
                [tempBtn setBackgroundImage:[UIImage imageNamed:[self getViewWithImageName:[valueKeys intValue]]] forState:UIControlStateNormal];
                tempBtn.hidden = NO;
                index ++;
            }
 
        }
    }
    for (int tempIndex =  index ; tempIndex < self.portArr.count ; tempIndex ++) {
        UIButton * tempBtn = (UIButton *)self.portArr[tempIndex];
        tempBtn.hidden = YES;
    }
}


- (NSString *)getViewWithImageName:(PORTCOLOR) TAG{
    
    if(TAG == PORTREDCOLOR){return @"Round-red";}
    else if (TAG == PORTGREENCOLOR){return @"Round-green";}
    else if (TAG == PORTYELLOWCOLOR){return @"Round-yellow";}
    else if (TAG == PORTBLUECOLOR){return @"Round-blue";}
    else if (TAG == PORTGRAYCOLOR){return @"Round-lightgrey";}
    else if (TAG == PORTBLACKCOLOR){return @"Round-black";}
    else if(TAG == PORTKNOWNCOLOR) {return @"dutou";}
    else
        return @"device_whiteCircle";
}

@end
