//
//  ACTouchPortForPanView.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/6/11.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ACTouchPortForPanViewForCallBack <NSObject>

/*!
 *   点击触摸模板上的颜色，然后回调到此类，给端口设置上对应的颜色
 *  @param tagIndex 端口颜色标记
 */

- (void)touchPanViewCallBackWithIndex:(NSInteger) tagIndex;

@end

@interface ACTouchPortForPanView : UIView

@property (strong,nonatomic) id<ACTouchPortForPanViewForCallBack> panViewDelegate;

@end


@interface ACTouchPortButton : UIButton

@end
