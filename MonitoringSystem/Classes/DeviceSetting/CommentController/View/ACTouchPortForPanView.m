//
//  ACTouchPortForPanView.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/6/11.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACTouchPortForPanView.h"
@interface ACTouchPortForPanView (){
    CGFloat objH;
    CGFloat objW;
}
@end


@implementation ACTouchPortForPanView

- (instancetype)init{
    if (self = [super init]) {
         objW = 120;
         objH = 260;
        self.bounds = CGRectMake(0, 0, objW, objH);
        [self setUpChildView];
    }
    return self;
}

- (void)setUpChildView{
    
    // 背景
    UIView * BG = [[UIView alloc] init];
    BG.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    BG.layer.cornerRadius = 5;
    BG.backgroundColor = [UIColor colorWithRed:48 / 255.0 green:51 / 255.0 blue:60 / 255.0 alpha:1];
    [self addSubview:BG];

    // 端口按钮
    for (int index = 0; index < 7; index++) {
        ACTouchPortButton * btnPort = [[ACTouchPortButton alloc] init];
        [btnPort setImage:[UIImage imageNamed:[self getBigViewWithImageName:index]] forState:UIControlStateNormal];
        [btnPort setTitle:[self getTitleWithImageName:index] forState:UIControlStateNormal];
        CGFloat space = index * objH / 7;
        btnPort.titleLabel.textAlignment = NSTextAlignmentRight;
        btnPort.titleLabel.font = [UIFont systemFontOfSize:12];
        btnPort.frame = CGRectMake(5, space, self.frame.size.width - 10 * 2, objH / 7);
        btnPort.tag = index;
        [self addSubview:btnPort];
        [btnPort addTarget:self action:@selector(selectPortCallBack:) forControlEvents:UIControlEventTouchUpInside];
        if(index > 0 && index <= 6){
            UIView * lineView = [[UIView alloc] initWithFrame:CGRectMake(10, space, self.frame.size.width - 10 * 2, 1)];
            lineView.backgroundColor = [UIColor whiteColor];
            lineView.alpha = 0.4;
            [self addSubview:lineView];
        }
    }
}

// 选中对应的端口颜色，回调上级，并设计颜色
- (void) selectPortCallBack:(UIButton *)sender{
    NSLog(@"选中的颜色是 %@",[self getBigViewWithImageName:(PORTCOLOR)sender.tag]);
    if ([self.panViewDelegate respondsToSelector:@selector(touchPanViewCallBackWithIndex:)]) {
        [self.panViewDelegate touchPanViewCallBackWithIndex:sender.tag];
    }
    
}

// 获取对应的端口颜色
- (NSString *)getBigViewWithImageName:(PORTCOLOR) TAG{
    
    if(TAG == PORTREDCOLOR){return @"Round-red";}
    else if (TAG == PORTGREENCOLOR){return @"Round-green";}
    else if (TAG == PORTYELLOWCOLOR){return @"Round-yellow";}
    else if (TAG == PORTBLUECOLOR){return @"Round-blue";}
    else if (TAG == PORTGRAYCOLOR){return @"Round-lightgrey";}
    else if (TAG == PORTBLACKCOLOR){return @"Round-black";}
    else
        return @"Round-gray";
}


// 获取对应的端口冲程
- (NSString *)getTitleWithImageName:(PORTCOLOR) TAG{
    if(TAG == PORTREDCOLOR){return @"0.003CC";}
    else if (TAG == PORTGREENCOLOR){return @"0.015CC";}
    else if (TAG == PORTYELLOWCOLOR){return @"0.020CC";}
    else if (TAG == PORTBLUECOLOR){return @"0.040CC";}
    else if (TAG == PORTGRAYCOLOR){return @"0.056CC";}
    else if (TAG == PORTBLACKCOLOR){return @"0.100CC";}
    else
        return NSLocalizedString(@"堵头",nil);
}
@end

@implementation ACTouchPortButton

- (CGRect)imageRectForContentRect:(CGRect)contentRect{
    
    CGFloat imgWith = 28;
    CGRect imageRect = CGRectMake(0, contentRect.size.height * 0.5 - imgWith * 0.5, imgWith, imgWith);
    return imageRect;
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect{
    CGRect titleRect = CGRectMake(contentRect.size.width - 75, contentRect.size.height * 0.5 - 8, 80, 16);
    return titleRect;
}

@end
