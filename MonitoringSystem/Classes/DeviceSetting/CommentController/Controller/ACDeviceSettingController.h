//
//  ACDeviceSettingController.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/28.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

/**
 *      泵芯转盘，设置出油量控制器
 */
#import <UIKit/UIKit.h>



@interface ACDeviceSettingController : UICollectionViewController

/*!
 *  设备编号
 */
@property (nonatomic,strong) NSString * imei;


/*!
 *  端口总数
 */
@property(nonatomic,assign) NSInteger deviceNumber;

/*!
 * 是否是管理者
 */
@property(nonatomic,assign) BOOL   isEmployer;

/*!
 * 如果是管理者，那么号码是多少
 */

@property(nonatomic,strong) NSString *  employer;

/*!
 * 管理者的区号
 */
@property(nonatomic,strong) NSString * zone;





@end
