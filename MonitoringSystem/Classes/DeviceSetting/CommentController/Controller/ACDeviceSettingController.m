//
//  ACDeviceSettingController.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/28.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
/*
   半透明的新手引导
    http://www.jianshu.com/p/b83aefdc9519
 */
#import "ACDeviceSettingController.h"
#import "ACDeviceSettingViewCell.h"
#import "UtilToolsClass.h"
#import "NetRequestClass.h"
#import "ACRecommedController.h"
#import "YLZHoledView.h"
#import "ACCustomAlertView.h"
@interface ACDeviceSettingController ()<UIAlertViewDelegate,DeviceSettingCellProtocol,YLZHoledViewDelegate,ACCustomAlertViewDelegate>{
    NSTimer * timer;// 引导手指来回移动
    NSInteger responeConfigDataLayer;// 服务器返回的ac配置的层数;
    int space;// 移动距离
    BOOL isPopPreView;// 是否需要跳转到上级页面
}
@property (nonatomic, weak) UIPageControl *control; // 下标指示器
@property (nonatomic,assign) NSInteger deviceLayer;//设置的层数
@property (nonatomic,assign) CGRect navRect;
@property (nonatomic,assign) CGRect statusRect;
@property(nonatomic,weak) ACDeviceSettingViewCell * settingCell;
@property (nonatomic, weak) YLZHoledView *holedView; //遮罩层
@property(nonatomic,weak) UIImageView * rightGuid; // 引导手势
@property(nonatomic,weak) UIImageView * guidView;// 引导手势

@end
@implementation ACDeviceSettingController

static NSString * const reuseIdentifier = @"ACDeviceSettingViewCell";
#pragma mark 初始化flowLayout
-(instancetype)init{
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc] init];
    if (self = [super init]) {
        //     设置尺寸
        layout.itemSize = [UIScreen mainScreen].bounds.size;
        //     设置间距
        layout.minimumLineSpacing = 0;
        //    设置方向
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    }
    return   [self initWithCollectionViewLayout:layout];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    isPopPreView = true;
    self.automaticallyAdjustsScrollViewInsets = NO;
    /*
        每一个cell都不适用复用。都要重新生成
     */
    [self.collectionView registerNib:[UINib nibWithNibName:@"ACDeviceSettingViewCell" bundle:nil] forCellWithReuseIdentifier:@"ACDeviceSettingViewCell0"];
     [self.collectionView registerNib:[UINib nibWithNibName:@"ACDeviceSettingViewCell" bundle:nil] forCellWithReuseIdentifier:@"ACDeviceSettingViewCell1"];
     [self.collectionView registerNib:[UINib nibWithNibName:@"ACDeviceSettingViewCell" bundle:nil] forCellWithReuseIdentifier:@"ACDeviceSettingViewCell2"];
    //     设置翻页效果
    self.collectionView.pagingEnabled = YES;
    //      设置反弹效果
    self.collectionView.bounces = NO;
    //     设置隐藏滚动条
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self setUpUI];
     //    获取配置
    [self getACConfigData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openSaveConfigMaskLayerForGuid6) name:@"openSaveMaskLayer" object:nil];

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [UD setObject:nil forKey:PORTCOLORTAGCOUNT];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}


#pragma mark 获取ac配置数据.
- (void) getACConfigData{
    [[UtilToolsClass getUtilTools] addDoLoading];
    __weak typeof(self) weakSelf = self;
    [NetRequestClass requestWithUrlForGetACConfig:self andUrl:[REQUESTHEADER stringByAppendingString:@"deviceList/loadConfigData.asp"] andParam:@{@"i_imei":self.imei} andiSFavoConfig:false success:^(NSDictionary *result) {
        [[UtilToolsClass getUtilTools] removeDoLoading];
        [weakSelf requestSuccessCallBack:result];
    } failure:^(NSString *failure) {
        [[UtilToolsClass getUtilTools] removeDoLoading];
        [UtilToolsClass addDisapperAlert:@"" withMessage:failure];
    }];
}



#pragma mark 请求成功后的回调
- (void) requestSuccessCallBack:(NSDictionary *)result{
    NSArray * listData = result[@"data"];
    responeConfigDataLayer = listData.count / 12; // 每一层都是12层的个数
    self.settingCell.configData = listData;
    if ([[UD objectForKey:@"maskLayerGuid3"] intValue]) {
        //  如果端口数为12，那么就直接开启下一个引导，无翻页效果，如果端口数为24以上，那么就存在翻页引导
        if (self.deviceNumber == 12) {
            // 直接进入引导4
            [UD setObject:@(0) forKey:@"maskLayerGuid3"];
            [UD setObject:@(1) forKey:@"maskLayerGuid4"];
            [self.settingCell openSelectPortMaskLayerForGuid4];
        }else{
            [self openScrollViewMaskLayerForGuid3];
        }
    }else{
        // 不存在引导，非管理员进入将弹框提示
        if (!self.isEmployer) {
            // 弹框
            ACCustomAlertView * alertView = [[ACCustomAlertView alloc] initAlertViewWithFrame:CGRectMake(SCREEN_WIDTH * 0.1, SCREEN_HEIGHT * 0.5 - 80, SCREEN_WIDTH * 0.8, 160) andSuperView:self.view];
            [alertView createTitle:NSLocalizedString(@"提示",nil) message:[NSString stringWithFormat:@"%@:+%@ %@%@",NSLocalizedString(@"该设备已被",nil),self.zone,self.employer,NSLocalizedString(@"用户绑定,设备配置修改后将无法保存",nil)]calcelButton:@"" okButton:NSLocalizedString(@"确定",nil)];
            [[UIApplication sharedApplication].keyWindow addSubview:alertView];
        }
    }
}


#pragma mark 询问用户是否需要保存设置，不需要退出，需要则保存数据
-(void)backItemClick{
    if ([[UD objectForKey:@"isEditDviceConfig"] intValue]) {
        if (self.isEmployer) {// 是管理员
            // 取消保存
            UIAlertView * isSaveAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示",nil) message:NSLocalizedString(@"保存当前的配置信息？",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"取消",nil) otherButtonTitles:NSLocalizedString(@"确定",nil), nil];
            isSaveAlert.tag = 0;
            [isSaveAlert show];
        }else{// 不是管理员
            [self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark 上传到服务器
-(void)saveItemClick{
    // 当前用户是管理者，保存，上传到服务器
    UIAlertView * isSaveAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示",nil)
                                                           message:NSLocalizedString(@"将当前配置加入到推荐配置中?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"取消",nil) otherButtonTitles:NSLocalizedString(@"确定",nil), nil];
    isSaveAlert.tag = 1;
    [isSaveAlert show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 0) {
        // 编辑过
        if (buttonIndex == 0) {
            // 取消保存
            [UD setObject:nil forKey:[NSString stringWithFormat:@"%@-%@",PORTDIC,[ACUserLoginManage shareInstance].userName]];
            [UD synchronize];
            [self.navigationController popViewControllerAnimated:YES];
            
        }else if(buttonIndex == 1){
            // 保存，上传到服务器,如果是引导模式，那么就return 不做处理
            [self uploadConfigToService:buttonIndex];
        }
    }
    else if (alertView.tag == 1){
            [self uploadConfigToService:!buttonIndex];
    }
}



#pragma mark 设置UI
- (void) setUpUI{
    
    self.settingCell.zone = self.zone;
    self.settingCell.employ = self.employer;
    self.settingCell.isEmploy = self.isEmployer;
    self.navigationItem.title = NSLocalizedString(@"泵芯配置", nil);
    UIBarButtonItem* backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back@3x"] style:UIBarButtonItemStylePlain target:self action:@selector(backItemClick)];
    self.navigationItem.leftBarButtonItem = backItem;
    if (self.isEmployer) {
//        UIBarButtonItem * saveItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"保存",nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveItemClick)];
        UIBarButtonItem * saveItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"OK"] style:UIBarButtonItemStylePlain target:self action:@selector(saveItemClick)];
        self.navigationItem.rightBarButtonItem = saveItem;
        self.navigationItem.hidesBackButton = YES;
    }
    self.collectionView.backgroundColor = [UIColor whiteColor];
    // 分割线
    UIView * speaceLineView = [[UIView alloc] initWithFrame:CGRectMake(0,  0,  ACStatusHieght(self).size.width, 1)];
    speaceLineView.backgroundColor = [UIColor whiteColor];
    speaceLineView.alpha = 0.3;
    [self.view addSubview:speaceLineView];
}



/*
#pragma mark 上传数据    注意：由于传入的后台接受格式为json。因此在请求方法内部做了动态修改参数，如果要添加参数，请在后面继续叠加.
                      注意：不要在控制器处转json格式。因为控制器不存在这样的业务操作
 */
- (void) uploadConfigToService:(NSInteger) uploadTag{
    NSString * tempDic =  [UD objectForKey:[NSString stringWithFormat:@"%@-%@",PORTDIC,[ACUserLoginManage shareInstance].userName]];
    [[UtilToolsClass getUtilTools] addDoLoading];
    NSDictionary * param = @{@"i_imei":self.imei,@"data":tempDic,@"isSave":[NSString stringWithFormat:@"%d",(int)uploadTag]};
    __weak typeof(self) weakSelf = self;
    [NetRequestClass requestWithURLForUploadACConfig:self andUrl:[REQUESTHEADER stringByAppendingString:@"deviceList/saveConfig.asp"] andParam:param success:^(NSInteger result) {
        [[UtilToolsClass getUtilTools] removeDoLoading];
        if (!result) {
            // 上传成功
            if (isPopPreView) {// 回推到上一次界面
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }else{// 开启新的引导
                [weakSelf openShowRecomdConfigMaskLayerForGuid7];
            }
        }else{
            [UtilToolsClass addDisapperAlert:@"" withMessage:@"保存失败"];
        }
    } failure:^(NSString *failure) {
        [[UtilToolsClass getUtilTools] removeDoLoading];
        [UtilToolsClass addDisapperAlert:@"" withMessage:failure];
    }];

}

#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.deviceNumber / 12;
}


/* iOS 10 方法  */
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    ACDeviceSettingViewCell *tempCell  = (ACDeviceSettingViewCell *)cell;
    tempCell.contentView.userInteractionEnabled = YES;
     [tempCell setUpWith:collectionView andIndex:tempCell.tag andPortLayer:(self.deviceNumber / 12)];
      self.settingCell = tempCell;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  
    NSString * reuseId = [NSString stringWithFormat:@"%@%ld",reuseIdentifier,(long)indexPath.row];
    ACDeviceSettingViewCell *setCell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseId forIndexPath:indexPath];
    setCell.tag = indexPath.row;
    setCell.contentView.userInteractionEnabled = YES;
     setCell.delegate = self;
    
    [setCell setUpWith:collectionView andIndex:indexPath.row andPortLayer:(self.deviceNumber / 12)];
     self.settingCell = setCell;
    return setCell;
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.collectionView.frame.size.width, self.collectionView.frame.size.height);
}


//调节每个item的edgeInsets代理方法
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake( 0, 0, 0, 0);
}



#pragma mark recommed delegate 
- (void)showRecommedCallBack{
   
        ACRecommedController * recommedController = [[ACRecommedController alloc] init];
        recommedController.imei = self.imei;
        recommedController.isEmployer = self.isEmployer;
        recommedController.employer = self.employer;
        recommedController.zone = self.zone;
        [recommedController returnText:^(NSDictionary *valueDic) {
            NSLog(@"valueDic :%@",valueDic);
            NSString * tagStr = [NSString stringWithFormat:@"%@",valueDic[@"tagStr"]];
            NSString * configPk =  [NSString stringWithFormat:@"%@",valueDic[@"configPk"]];
            if (valueDic.count == 0 || [tagStr isEqualToString:@""] || [configPk isEqualToString:@""]) {
                // 有问题，回来
                if([[UD objectForKey:@"maskLayerGuid8"] intValue]){
                    [self openBackMaskLayerForGuid8];
                }
                return ;
            }
            [self requestRecommdConfigWithPk:valueDic];
        }];
        [self presentViewController:recommedController animated:YES completion:nil];
}


#pragma mark 获取
- (void) requestRecommdConfigWithPk:(NSDictionary *) dic{
    
    [[UtilToolsClass getUtilTools] addDoLoading];
    __weak typeof(self) weakSelf = self;
    [NetRequestClass requestWithUrlForGetACConfig:self andUrl:[REQUESTHEADER stringByAppendingString:@"deviceList/loadConfigData.asp"]  andParam:@{@"state":dic[@"tagStr"],@"configPk":dic[@"configPk"]} andiSFavoConfig:false success:^(NSDictionary *result) {
        [[UtilToolsClass getUtilTools] removeDoLoading];
       [UD setObject:@(0) forKey:@"isEditDviceConfig"];
        NSArray *resultArr = result[@"data"];
        responeConfigDataLayer = resultArr.count / 12; // 每一层都是12层的个数
        weakSelf.deviceNumber = resultArr.count ;// 选中的配置的端口数
        weakSelf.settingCell.configData = resultArr; // 设置数据源
        [weakSelf.collectionView reloadData]; // 刷新数据
        [UD setObject:@(1) forKey:@"isEditDviceConfig"]; // 设置为编辑状态
        if([[UD objectForKey:@"maskLayerGuid8"] intValue]){
            [weakSelf openBackMaskLayerForGuid8];
        }
    } failure:^(NSString *failure) {
        [[UtilToolsClass getUtilTools] removeDoLoading];
        [UtilToolsClass addDisapperAlert:@"" withMessage:failure];
    }];
}



#pragma mark 引导翻页效果
/**
 *  引导3有两种方向-----如果是12的端口，直接忽视，如果是24个以上端口，存在引导
 */
- (void) openScrollViewMaskLayerForGuid3{
   
    YLZHoledView * tempHoldView = [[YLZHoledView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    tempHoldView.holeViewDelegate = self;
    UIImageView * rightGuid = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Guidjiangtou"]];
    rightGuid.center = CGPointMake(SCREEN_WIDTH * 0.75, SCREEN_HEIGHT * 0.45);
    [rightGuid sizeToFit];
    [tempHoldView addSubview:rightGuid];
    self.rightGuid = rightGuid;
    
    UIImageView * guidView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Guidshouzhi"]];
    guidView.center = CGPointMake(SCREEN_WIDTH * 0.8, SCREEN_HEIGHT * 0.5 );
    [guidView sizeToFit];
    [guidView.layer addAnimation:[self moveX:1 X:[NSNumber numberWithFloat: - 40]] forKey:nil];
    [tempHoldView addSubview:guidView];
    self.guidView = guidView;
    // 引导的文字
    UIImageView * guidLabelView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Guidhuadong"]];
    [guidLabelView sizeToFit];
    guidLabelView.layer.anchorPoint = CGPointMake(1, 1);
    guidLabelView.center = CGPointMake(SCREEN_WIDTH * 0.85, SCREEN_HEIGHT * 0.6);
    [tempHoldView addSubview:guidLabelView];
    
    
    // 添加手势
    UISwipeGestureRecognizer * swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipGesture:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [tempHoldView addGestureRecognizer:swipeGesture];
    
    [[UIApplication sharedApplication].keyWindow addSubview:tempHoldView];
    self.holedView = tempHoldView;

}


-(CABasicAnimation *)moveX:(float)time X:(NSNumber *)x{
     CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];///.y的话就向下移动。
     animation.toValue = x;
     animation.duration = time;
        animation.removedOnCompletion = YES;//yes的话，又返回原位置了。
        animation.repeatCount = MAXFLOAT;
        animation.fillMode = kCAFillModeForwards;
        return animation;
}


// 左边扫描
- (void)leftSwipGesture:(UISwipeGestureRecognizer *) swipeGesture{
    [self.holedView removeGestureRecognizer:swipeGesture];
    [self.guidView removeFromSuperview];
    [self.rightGuid removeFromSuperview];
 
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    // 重新指引方向
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UIImageView * leftGuid = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Guidjiangtou"]];
        leftGuid.center = CGPointMake(SCREEN_WIDTH * 0.25, SCREEN_HEIGHT * 0.45);
        leftGuid.transform = CGAffineTransformMakeScale(-1, 1);
        [self.holedView addSubview:leftGuid];
        UIImageView * guidView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Guidshouzhi"]];
        guidView.center = CGPointMake(SCREEN_WIDTH * 0.2, SCREEN_HEIGHT * 0.5 );
        [guidView sizeToFit];
        [guidView.layer addAnimation:[self moveX:1 X:[NSNumber numberWithFloat: 40]] forKey:nil];
        [self.holedView addSubview:guidView];
        // 添加手势
        UISwipeGestureRecognizer * swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipGesture:)];
        swipe.direction = UISwipeGestureRecognizerDirectionRight;
        [self.holedView addGestureRecognizer:swipe];

    });

}

- (void)rightSwipGesture:(UISwipeGestureRecognizer *) swipeGesture{
    [self.holedView removeGestureRecognizer:swipeGesture];
    [self.holedView removeFromSuperview];
    // 界面左边滚动
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //开启引导
        [UD setObject:@(0) forKey:@"maskLayerGuid3"];
        [UD setObject:@(1) forKey:@"maskLayerGuid4"];
        [self.settingCell openSelectPortMaskLayerForGuid4];
 
    });
   
}

/**
 *  开启引导6,引导保存
 */
- (void) openSaveConfigMaskLayerForGuid6{
    if ([[UD objectForKey:@"maskLayerGuid6"] intValue]) {
        //添加遮罩指示
        YLZHoledView *  maskView = [[YLZHoledView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        maskView.holeViewDelegate = self;
        // 圆形
         [maskView initWithMaskLayerWithObj:CGRectMake(SCREEN_WIDTH - 80, 10, 80, 80) layerRadius:40 fingerRect:CGPointMake(SCREEN_WIDTH - 10,80) holdViewTag:0];
        
        UIImageView * guidLabelView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Guidbaocun"]];
        [guidLabelView size];
        guidLabelView.center = CGPointMake(SCREEN_WIDTH - 80, 100);
        guidLabelView.layer.anchorPoint = CGPointMake(1,1);
        [maskView addSubview:guidLabelView];
        self.holedView = maskView;
        [[UIApplication sharedApplication].keyWindow addSubview:maskView];
        
    }
}


/**
 *  开启引导7,显示推荐配置
 */
- (void) openShowRecomdConfigMaskLayerForGuid7{
    if ([[UD objectForKey:@"maskLayerGuid7"] intValue]) {
        //添加遮罩指示
        YLZHoledView * tempMask = [[YLZHoledView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        tempMask.holeViewDelegate = self;
        UIImageView * guidLabelView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Guidtuijianpeizhi"]];
        [guidLabelView size];
        guidLabelView.center = CGPointMake(SCREEN_WIDTH - 100, SCREEN_HEIGHT - 80);
        guidLabelView.layer.anchorPoint = CGPointMake(1,1);
        [tempMask addSubview:guidLabelView];
        // 圆形
        [tempMask initWithMaskLayerWithObj:CGRectMake(SCREEN_WIDTH - 80, SCREEN_HEIGHT - 80, 80, 80) layerRadius:40 fingerRect:CGPointMake(SCREEN_WIDTH - 10,SCREEN_HEIGHT - 5) holdViewTag:1];
     
        [[UIApplication sharedApplication].keyWindow addSubview:tempMask];
        self.holedView = tempMask;
    }
}



/**
 *  开启引导8,显示返回按钮
 */
- (void) openBackMaskLayerForGuid8{
    //添加遮罩指示
    YLZHoledView * tempMask = [[YLZHoledView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    tempMask.holeViewDelegate = self;
    // 圆形
    UIImageView * guidLabelView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Guidfanhui"]];
    [guidLabelView size];
    guidLabelView.center = CGPointMake(60, 100);
    guidLabelView.layer.anchorPoint = CGPointMake(0,0);
    [tempMask addSubview:guidLabelView];
     [tempMask initWithMaskLayerWithObj:CGRectMake(0, 10, 60, 60) layerRadius:40 fingerRect:CGPointMake(40,80) holdViewTag:2];
    [[UIApplication sharedApplication].keyWindow addSubview:tempMask];
    self.holedView = tempMask;
    
}



#pragma mark 遮罩层的委托回调
- (void)holedView:(YLZHoledView *)holedView didSelectHoleAtIndex:(NSUInteger)index{
    if (holedView.tag == 0) {
        if (index == 0) {
            [self.holedView removeFromSuperview];
            ACCustomAlertView * alertView = [[ACCustomAlertView alloc] initAlertViewWithFrame:CGRectMake(SCREEN_WIDTH * 0.1, SCREEN_HEIGHT * 0.5 - 80, SCREEN_WIDTH * 0.8, 140) andSuperView:self.view];
            alertView.delegate = self;//需要保存当前的配置信息？
            [alertView createTitle:NSLocalizedString(@"提示",nil) message:NSLocalizedString(@"将当前配置加入到推荐配置中?",nil) calcelButton:NSLocalizedString(@"取消",nil) okButton:NSLocalizedString(@"确定",nil)];
            [[UIApplication sharedApplication].keyWindow addSubview:alertView];
        }
    }else if (holedView.tag == 1){
        if (index == 0) {
            //存在管理者 UIAlertView
            [self.holedView removeFromSuperview];
            [UD setObject:@(0) forKey:@"maskLayerGuid7"];
            [UD setObject:@(1) forKey:@"maskLayerGuid8"];
            [self showRecommedCallBack];
        }
    }else if (holedView.tag == 2){
        if (index == 0) {
            [UD setObject:@(0) forKey:@"maskLayerGuid8"];
            [UD setObject:@(1) forKey:@"maskLayerGuid9"];
            [self.holedView removeFromSuperview];
            self.collectionView.scrollEnabled = YES;
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(void)customAlertView:(ACCustomAlertView *)customAlertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    isPopPreView = false;
    [UD setObject:@(0) forKey:@"maskLayerGuid6"];
    [UD setObject:@(1) forKey:@"maskLayerGuid7"];
    [self uploadConfigToService:buttonIndex];
}
@end
