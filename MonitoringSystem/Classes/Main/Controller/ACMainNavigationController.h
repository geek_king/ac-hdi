//
//  ACMainNavigationController.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/25.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACMainNavigationController : UINavigationController{
     BOOL _changing;
}

@property(nonatomic, retain)UIView *alphaView;
//-(void)setAlph;

@end
