//
//  ACUpdateVersionController.m
//  MonitoringSystem
//
//  Created by FreedomCoco on 8/11/16.
//  Copyright © 2016 Interlube. All rights reserved.
//

#import "ACUpdateVersionController.h"
#import "JPUSHService.h"
@interface ACUpdateVersionController ()

@end

@implementation ACUpdateVersionController

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [JPUSHService startLogPageView:@"ACUpdateVersionController"];
//         [MobClick beginLogPageView:@"ACUpdateVersionController"];
        
        [self setUpChildView];
    }
    return self;
}



- (void)setUpChildView{
    // 背景半透明
    CGFloat imageViewX = SCREEN_WIDTH * 0.12;
    CGFloat imageViewY = 80;
    CGFloat imageViewW = SCREEN_WIDTH * 0.76;
    CGFloat imageViewH = 350;
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    UIImageView * imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gengxin"]];
    imageView.frame = CGRectMake(imageViewX,  imageViewY, imageViewW, imageViewH);
    imageView.userInteractionEnabled = YES;
    
    UILabel * newVersion = [[UILabel alloc] initWithFrame:CGRectMake(0, imageViewH * 0.5 - 20, imageViewW, 40)];
    newVersion.text = NSLocalizedString(@"发现新版本", nil);
    newVersion.textColor = [UIColor colorWithRed:231 / 255.0 green:93 / 255.0 blue:36 / 255.0 alpha:1];
    newVersion.font = [UIFont systemFontOfSize:24];
    newVersion.textAlignment = NSTextAlignmentCenter;
    [imageView addSubview:newVersion];
    
    UILabel * updateInfo = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(newVersion.frame), imageViewW, 30)];
    updateInfo.text = NSLocalizedString(@"更新说明",nil);
    updateInfo.font = [UIFont systemFontOfSize:12];
    updateInfo.textColor = [UIColor colorWithRed:68 / 255.0 green:68 / 255.0 blue:68 / 255.0 alpha:1];
     updateInfo.textAlignment = NSTextAlignmentCenter;
    [imageView addSubview:updateInfo];
    
    
    UILabel * sofwareInfo = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(updateInfo.frame), imageViewW, 30)];
    sofwareInfo.text = NSLocalizedString(@"优化软件的兼容性",nil);
    sofwareInfo.font = [UIFont systemFontOfSize:12];
    sofwareInfo.textAlignment = NSTextAlignmentCenter;
    sofwareInfo.textColor = [UIColor colorWithRed:68 / 255.0 green:68 / 255.0 blue:68 / 255.0 alpha:1];
    [imageView addSubview:sofwareInfo];
    
    
    // 立即更新
    UIButton * updateNow = [[UIButton alloc] initWithFrame:CGRectMake(imageViewW * 0.5 - 50,  imageViewH - 60, 100, 30)];
    [updateNow setBackgroundImage:[UIImage imageNamed:@"anniu"] forState:UIControlStateNormal];
    [updateNow setTitle:NSLocalizedString(@"立即更新", nil) forState:UIControlStateNormal];
    updateNow.titleLabel.font = [UIFont systemFontOfSize:14];
    updateNow.tag = 0;
    [imageView addSubview:updateNow];
    [updateNow addTarget:self action:@selector(buttonCallBack:) forControlEvents:UIControlEventTouchUpInside];
    
    // wifi
    UILabel * tips = [[UILabel alloc] initWithFrame:CGRectMake(0, imageViewH - 20, imageViewW, 20)];
    tips.textAlignment = NSTextAlignmentCenter;
    tips.textColor = [UIColor whiteColor];
    tips.text = NSLocalizedString(@"WIFI环境下更新更加快捷", nil);
    tips.font = [UIFont systemFontOfSize:10];
    [imageView addSubview:tips];
    

    // 关闭
    UIButton * coloseBtn = [[UIButton alloc] initWithFrame:CGRectMake(imageViewW - 40,  imageViewY  + 50, 20, 20)];
    [coloseBtn setBackgroundImage:[UIImage imageNamed:@"guanbi"] forState:UIControlStateNormal];
    coloseBtn.tag = 1;
    [imageView addSubview:coloseBtn];
    [self addSubview:imageView];
    [coloseBtn addTarget:self action:@selector(buttonCallBack:) forControlEvents:UIControlEventTouchUpInside];
}

- (void) buttonCallBack:(UIButton *)sender{
     [self removeFromSuperview];
    if (sender.tag == 0){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:DOWLOAD_APP]];
    }
    
}

@end
