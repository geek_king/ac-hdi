//
//  MacroMethod.h
//  MonitoringSystem
//
//  Created by Kang on 16/4/19.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

//#ifndef MacroMethod_h
//#define MacroMethod_h



/// 宏定义 匿名 block
#define  REQUEST_CALLBACK void (^)(NetResponse * response)

/** 处理数据中的null值 */
#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj);  __obj == [NSNull null] ?  @0 : obj;})
/**__weak  */
#define menuWeakSelf(menuWeakSelf)  __weak __typeof(&*self)menuWeakSelf = self;
/**获取当前语言 */
#define CurrentLanguage ([[NSLocale preferredLanguages] objectAtIndex:0])

//使用ARC和不使用ARC
#if __has_feature(objc_arc)
//compiling with ARC
#else
// compiling without ARC
#endif



/** 颜色宏 */
// RGB颜色  -UIColor
#define RGB(r,g,b,a) \
[UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

#define ELCellRandomColor \
[UIColor colorWithRed:arc4random_uniform(100)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(100)/255.0 alpha:0.9f] //随机颜色

// 带有RGBA的颜色设置
#define COLOR(R, G, B, A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]

// rgb颜色转换（16进制->10进制）
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


/** 图片宏 */
// 读取本地图片
#define LOADIMAGE(file,ext) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:file ofType:ext]]

// 定义UIImage对象
#define IMAGE(A) [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:A ofType:nil]]

// 定义UIImage对象
#define ImageNamed(_pointer) [UIImage imageNamed:[UIUtil imageName:_pointer]]


/**
 * 获取界面各种高度
 */
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)          //获取手机设备的高度
#define SCREEN_WIDTH  ([UIScreen mainScreen].bounds.size.width)           //获取手机设备的宽度
#define SCREEN_NAVIGATIONBAR_HEIGHT  self.navigationController.navigationBar.frame.size.height // 导航条高度 - 44
#define SCREEN_TAB_BAR  49            // TabBar高度 - 49
#define SCREEN_STATUS_BAR  20         // 状态栏 高度 - 20
#define SCREEN_FRAME (CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT))







//获取ios版本
/// 系统版本 = __IPHONE_0_0
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
/// 系统版本 > __IPHONE_0_0
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
/// 系统版本 >= __IPHONE_0_0
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
/// 系统版本 < __IPHONE_0_0
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
/// 系统版本 <= __IPHONE_0_0
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
///  floatvlue
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define CurrentSystemVersion [[UIDevice currentDevice] systemVersion]


/// 宽度 = SCREEN_WIDTH \ 高度 = SCREEN_HEIGHT
#define SYSTEM_VERSION_EQUAL_TO_W_orH(WH,wh)                               ([[NSString stringWithFormat:@"%f", WH] compare:wh options:NSNumericSearch] == NSOrderedSame)
/// 宽度 > SCREEN_WIDTH \ 高度 > SCREEN_HEIGHT
#define SYSTEM_VERSION_GREATER_THAN_W_orH(WH,wh)                           ([[NSString stringWithFormat:@"%f", WH] compare:wh options:NSNumericSearch] == NSOrderedDescending)
/// 宽度 >= SCREEN_WIDTH \ 高度 >= SCREEN_HEIGHT
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO_orH(WH,wh)                 ([[NSString stringWithFormat:@"%f", WH] compare:wh options:NSNumericSearch] != NSOrderedAscending)
/// 宽度 < SCREEN_WIDTH \ 高度 < SCREEN_HEIGHT
#define SYSTEM_VERSION_LESS_THAN_W_orH(WH,wh)                              ([[NSString stringWithFormat:@"%f", WH] compare:wh options:NSNumericSearch] == NSOrderedAscending)
/// 宽度 <= SCREEN_WIDTH \ 高度 <= SCREEN_HEIGHT
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO_W_orH(WH,wh)                  ([[NSString stringWithFormat:@"%f", WH] compare:wh options:NSNumericSearch] != NSOrderedDescending)






/* Mob SMS
 */
#define MOBSMSAPPKEY @"125dd46b1790f"//  125dd46b1790f
#define MOBSMSAPPSECRET @"b6b7c0f1f7577cdfc1669221667ab2cf"//  b6b7c0f1f7577cdfc1669221667ab2cf

/**
 *  友盟 错误日志统计
 */
#define UM_CRACHKEY  @"57b11bd067e58eaf05003655"


#define ITTDEBUG
#define ITTLOGLEVEL_INFO     10
#define ITTLOGLEVEL_WARNING  3
#define ITTLOGLEVEL_ERROR    1

#ifndef ITTMAXLOGLEVEL

#ifdef DEBUG
#define ITTMAXLOGLEVEL ITTLOGLEVEL_INFO
#else
#define ITTMAXLOGLEVEL ITTLOGLEVEL_ERROR
#endif




/**
 * 单例化一个类
 */
#define SYNTHESIZE_SINGLETON_FOR_CLASS(classname) \
\
static classname *shared##classname = nil; \
\
+ (classname *)shared##classname \
{ \
@synchronized(self) \
{ \
if (shared##classname == nil) \
{ \
shared##classname = [[self alloc] init]; \
} \
} \
\
return shared##classname; \
} \
\
+ (id)allocWithZone:(NSZone *)zone \
{ \
@synchronized(self) \
{ \
if (shared##classname == nil) \
{ \
shared##classname = [super allocWithZone:zone]; \
return shared##classname; \
} \
} \
\
return nil;\
}\
\
- (id)copyWithZone:(NSZone *)zone \
{ \
return self; \
}

//G－C－D
#define BACK(block) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block)
#define MAIN(block) dispatch_async(dispatch_get_main_queue(),block)


#endif /* MacroMethod_h */





/* ##################   JK start  ################## */



#if DEBUG
#define  NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...)
#endif
// 状态栏的高度
#define ACNavHieght  [[UIApplication sharedApplication] statusBarFrame]
// 导航条的高度
#define ACStatusHieght(obj)  obj.navigationController.navigationBar.frame
//公共的颜色
#define ACCommentColor [UIColor colorWithRed:36/ 255.0 green:40 / 255.0 blue:52 / 255.0 alpha:1]
// 主窗体
#define  ACApplicationKeyWindow [UIApplication sharedApplication].keyWindow



// 单例-m文件
#define SINGLETON_M(FUNC_NAME) + (instancetype)FUNC_NAME { \
static id singleton = nil; \
static dispatch_once_t one; \
dispatch_once(&one, ^{ \
singleton = [[self alloc] init]; \
}); \
return singleton; \
}

// 单例-h文件
#define SINGLETON_H(FUNC_NAME) + (instancetype)FUNC_NAME;


#define UD  [NSUserDefaults standardUserDefaults]

typedef enum{
    PORTWHILTECOLOR = -1, // 白
    PORTREDCOLOR, // 红
    PORTGREENCOLOR, // 绿
    PORTYELLOWCOLOR, // 黄
    PORTBLUECOLOR, // 蓝
    PORTGRAYCOLOR, // 灰
    PORTBLACKCOLOR, // 黑
    PORTKNOWNCOLOR // 关闭
    
}PORTCOLOR;

/**
 *  // 保存cookit字典
 *
 *  以下的四个都是用于添加到网络请求修改头部请求头的操作
 */
#define  acLocalCookieName @"MyProjectCookie"
#define  acLocalUserData @"MyProjectLocalUser"
#define acServerSessionCookie @"JSESSIONID"
#define acCookitDict  @"cookitDict"
/**
 *  友盟 错误日志统计
 */
#define UM_CRACHKEY  @"57b11bd067e58eaf05003655"


#define JPushAppKey @"735c3d581d2e37244a3c218b"
#define JPushChannel @"App Store"
//0 (默认值)表示采用的是开发证书，1 表示采用生产证书发布应用。
#define JPushApsForProduction 1

// 端口字典，保存了端口的位置、颜色、层数
#define PORTDIC  @"portDic"
// 端口颜色的总数
#define PORTCOLORTAGCOUNT @"portColorTagCount"
#define ACDocumentPath   [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
#define ACProductPricePath [[NSBundle mainBundle] pathForResource:@"ACProductPriceList.plist" ofType:nil]


typedef void(^GetDeviceConfig)(NSDictionary * valueDic);
typedef void(^PushDeviceName)(NSString * cusDeviceName);
typedef void(^ReturnCallBack)();
/* ##################   JK  end  ##################      */




