//
//  NavigationC.m
//  DataStatistics
//
//  Created by Kang on 15/10/9.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "NavigationC.h"

@interface NavigationC ()

@end

@implementation NavigationC


#pragma mark 一个类只会调用一次
+ (void)initialize {
    [self setupNavigationBarTheme];
    [self setupBarButtonItemTheme];
}


// 设置Bar样式
+ (void)setupNavigationBarTheme {
    //  2、在info中提交一个字段View controller-based status bar appearance 值为NO。
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    UINavigationBar *navBar = [UINavigationBar appearanceWhenContainedIn:[NavigationC class], nil];//针对NavigationC  修改
    // 设置文字属性
#ifdef __IPHONE_7_0
    // 字体颜色 , 字体大小
    [navBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName : [UIFont boldSystemFontOfSize:16]}];
#else
    // 字体颜色 ,阴影
    [navBar  setTitleTextAttributes:@{UITextAttributeTextColor : [UIColor whiteColor], NSShadowAttributeName : [[NSShadow alloc] init]}];
#endif

    // 设置navigationbar的半透明效果
    [navBar setTranslucent:NO];
    // 设置bar的文字颜色
    navBar.tintColor = [UIColor whiteColor];
    
    // 设置bar的背景颜色(Load resources for iOS 7 or later   )
    navBar.barTintColor = ACCommentColor;

}




/// 设置Itemi样式
+ (void)setupBarButtonItemTheme {
//     通过appearance对象能修改整个项目中所有UIBarButtonItem的样式
//    UIBarButtonItem *barBtn = [UIBarButtonItem appearance]; //所有
//    /** 按钮背景 */
//    [barBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
}

/// 控制器创建View 完成调用


- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:YES];
}


- (void)back {
    // 返回
    [self popViewControllerAnimated:YES];
}



@end
