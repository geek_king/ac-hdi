//
//  ACCustomTextFieid.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/20.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACCustomTextFieid.h"

@implementation ACCustomTextFieid

////控制placeHolder的位置，左右缩20
//-(CGRect)placeholderRectForBounds:(CGRect)bounds
//{
//   
//    CGRect inset = CGRectMake(bounds.origin.x+100, bounds.origin.y, bounds.size.width -10, bounds.size.height);
//    return inset;
//}
//
//控制显示文本的位置
-(CGRect)textRectForBounds:(CGRect)bounds
{
    
    CGRect inset = CGRectMake(bounds.size.width * 0.3 - 20, bounds.origin.y, bounds.size.width -10, bounds.size.height);
    return inset;
    
}
//控制编辑文本的位置
-(CGRect)editingRectForBounds:(CGRect)bounds
{

    CGRect inset = CGRectMake(bounds.size.width * 0.3 - 20, bounds.origin.y, bounds.size.width -10, bounds.size.height);
    return inset;
}


@end