//
//  ACCustomButton.m
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/21.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACCustomButton.h"

@implementation ACCustomButton

-(CGRect)imageRectForContentRect:(CGRect)contentRect{
   
    CGFloat imageX = contentRect.size.width - 30;
    return CGRectMake(imageX,5, 30, 30);
}

- (CGRect) titleRectForContentRect:(CGRect)contentRect{
      CGFloat titleW = contentRect.size.width - 30;
    return CGRectMake(10,0, titleW, 40);
}

@end
