//
//  ACBaseDataSource.m
//  MonitoringSystem
//  Created by JiaKang.Zhong on 16/4/28.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
#import "ACBaseDataSource.h"
#import "ACDataEntity.h"
#import "ACDeviceListModel.h"
#import "ACDeviceListModelCenter.h"
@implementation ACBaseDataSource

- (NSMutableArray *)dataModelArray{
    if (!_dataModelArray) {
        _dataModelArray = [NSMutableArray array];
    }
    return _dataModelArray;
}

+ (instancetype) dataSourceWithEntity:(ACDataEntity *)entity{
    
    return [[self alloc] initWithEntity:entity];
}
- (instancetype) initWithEntity:(ACDataEntity *)entity{
    if (self = [super init]) {
        ACDeviceListModelCenter * centerModel = (ACDeviceListModelCenter * )entity;
        _dataModelArray =  [centerModel.allDevice mutableCopy];
    }
    return self;
}




+ (instancetype) dataSourceWithEntity:(ACDataEntity *)entity andWithDeviceState:(NSInteger )state{
    
    return [[self alloc] initWithEntity:entity andWithDeviceState:state];
}

- (instancetype) initWithEntity:(ACDataEntity *)entity andWithDeviceState:(NSInteger )state{
    if (self = [super init]) {
        ACDeviceListModelCenter * centerModel = (ACDeviceListModelCenter * )entity;
        if (state == 0) {
            _dataModelArray =  [centerModel.allDevice mutableCopy];
        }else if (state == 1) {
            _dataModelArray =  [centerModel.normarlDevice mutableCopy];
        }else if (state == 2){
            _dataModelArray =  [centerModel.breakDownDevice mutableCopy];
        }
    }
    return self;
}




// 多少组
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


// 多少行
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataModelArray.count;
}

// 每一行长什么样，交给子类去实现
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return nil;
}

@end
