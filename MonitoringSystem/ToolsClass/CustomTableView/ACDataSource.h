//
//  ACDataSource.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/4/28.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ACBaseDataSource.h"


@interface ACDataSource : ACBaseDataSource

/*!
 *  因为集成nsobject。在需要的跳转的时候，可以采用
 */
@property(nonatomic,weak) UIViewController * superController;
@end
