//
//  ACDataSource.m
//  MonitoringSystem
//  Created by JiaKang.Zhong on 16/4/28.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
#import "ACDataSource.h"
#import "ACDeviceListModel.h"
#import "ACDeviceViewCell.h"
#import "NetRequestClass.h"
#import "UtilToolsClass.h"
#import "ACUserLoginManage.h"
#import "ACChangeAuthorController.h"
@interface ACDataSource () <ACDeviceViewCellDelegate>

@end

@implementation ACDataSource

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ACDeviceViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"acDeviceViewCell" forIndexPath:indexPath];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ACDeviceViewCell" owner:nil options:nil] lastObject];
    }
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    ACDeviceListModel * slistModel =  self.dataModelArray[indexPath.row];
    cell.listModel = slistModel;
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

// 选中cell后的回调
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ACDeviceListModel * slistModel =  self.dataModelArray[indexPath.row];
    self.didSelectCellClick(2,slistModel.code);
}


// 返回单元格的风格
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewCellEditingStyleDelete;
}

-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //        获取选中行索引值
        ACDeviceListModel * listModel =   self.dataModelArray[indexPath.row];
        if (listModel.isEmployer) {
            // 如果当前用户是管理员，如果此台设备只有一个人绑定，就直接解绑，如果多人绑定就必须要转移权限后，才可以解除绑定
            if(listModel.bindings == 1){
                [self requestRemoveBing:listModel tableView:tableView indexPath:indexPath];
            }else{
                ACChangeAuthorController * changeAuthorController = [[ACChangeAuthorController alloc] init];
                changeAuthorController.imei = listModel.code;
                changeAuthorController.isDeviceInfoEnter = NO;
                changeAuthorController.bindings = listModel.bindings;
                [self.superController.navigationController pushViewController:changeAuthorController animated:YES];
            }
        }else{
            [self requestRemoveBing:listModel tableView:tableView indexPath:indexPath];
        }
   }
}


- (void) requestRemoveBing:(ACDeviceListModel *)listModel tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    // 发送解绑请求
    __weak typeof(self) weakSelf = self;
    [NetRequestClass requestWithUrlForUnBindingDevice:nil andUrl:[REQUESTHEADER stringByAppendingString:@"deviceList/deviceRemoveBinding.asp"] andParam:@{@"i_imei":listModel.code,@"bindingCount":@(listModel.bindings)} success:^(NSInteger result) {
        if (!result ) {
            [weakSelf.dataModelArray removeObject:listModel];
            //  删除单元格的某一行时，在用动画效果实现删除过程
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [UtilToolsClass addDisapperAlert:@"" withMessage:NSLocalizedString(@"设备解绑成功",nil)];
            // 头部进行刷新
            [tableView.mj_header beginRefreshing];
        }
    } failure:^(NSString *failure) {
        [UtilToolsClass addDisapperAlert:@"" withMessage:failure];
    }];

}

- (void)cellDelegateCallBackWith:(NSInteger)enterTag imei:(NSString *)imei{
    self.didSelectCellClick(enterTag,imei);
}
@end
