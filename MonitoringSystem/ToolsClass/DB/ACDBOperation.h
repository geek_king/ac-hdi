//
//  ACDBOperation.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/12.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

/**
 *   此类为数据库操作类，提供创建表、增加、删除、查找的功能，如需要更多功能，请在后续按照原有的格式添加。
     数据库采用第三方FMDB操作，非原生sqlit3
 */
#import <Foundation/Foundation.h>
@class ACDataEntity;
@interface ACDBOperation : NSObject

SINGLETON_H(dbOperation);

/**
 *  创建表。如果不存在数据库，将会创建指定名称的数据库
 *
 *  @param dbName    数据库名字
 *  @param tableName 表名
 
 注意：设备标示为唯一编号
 */
- (void) createWithTableForDB:(NSString *)dbName andTableName:(NSString *)tableName result:(void(^)(BOOL result))result;




/**
 *  插入数据
 *
 *  @param tableName    表名
 *  @param deviceEntity 对象实体
 *  @param result      结果返回
 */
- (void)insertDataWithTableName:(NSString *)tableName andEmityObj:(ACDataEntity *)dataEntity result:(void(^)(NSError *error))result;



/**
 *  查询设备
 *
 *  @param tableName
 *  @param deviceIndentifies 设备标示
 *  @param result
 */
- (NSArray<ACDataEntity *> *) queryDataWithTableName:(NSString *)tableName andIdentifies:(NSString *)deviceIndentifies result:(void(^)(NSError *error))result;



/**
 *  删除设备
 *
 *  @param tableName
 *  @param deviceIndentifies
 *  @param result            
 */
- (void) deleteDataWithTableName:(NSString *)tableName andIdentifies:(NSString *)deviceIndentifies result:(void(^)(NSError *error))result;

@end
