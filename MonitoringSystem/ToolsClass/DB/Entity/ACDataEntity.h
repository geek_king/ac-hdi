//
//  ACDataEntity.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/13.
//  Copyright © 2016年 YTYangK. All rights reserved.
//


/*
        数据实体类，如果需要进行数据库操作的模型对象，都继承此类。
        原理：里氏替换原则
 */
#import <Foundation/Foundation.h>

@interface ACDataEntity : NSObject


@end
