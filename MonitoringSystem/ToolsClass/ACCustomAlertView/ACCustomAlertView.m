//
//  YXCustomAlertView.m
//  YXCustomAlertView
//
//  Created by Houhua Yan on 16/7/12.
//  Copyright © 2016年 YanHouhua. All rights reserved.

//

#import "ACCustomAlertView.h"
#import "MLLabel.h"
#import "MLLinkLabel.h"

@interface ACCustomAlertView(){
    BOOL isNeedCallBack;
}
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) MLLinkLabel *linkLabel;
@end

@implementation ACCustomAlertView


- (UIView *) middleView
{
    if (!_middleView) {
        _middleView = [[UIView alloc] init];
        _middleView.backgroundColor = [UIColor blackColor];
        _middleView.alpha = 0.65;
    }
    return _middleView;
}

- (UILabel *) titleLabel{
    
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont boldSystemFontOfSize:18];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.backgroundColor = [UIColor clearColor];
        
    }
    return _titleLabel;
}

- (UILabel *)linkLabel{
    if (!_linkLabel) {
        _linkLabel = [[MLLinkLabel alloc] init];
        _linkLabel.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame), self.frame.size.width , 60);
        _linkLabel.numberOfLines = 3;
    }
    return _linkLabel;
}



#pragma mark - getter And setter
- (void) setTitleStr:(NSString *)titleStr
{
    _titleLabel.text = titleStr;
}


- (void)setContentStr:(NSString *)contentStr{
    _linkLabel.text = contentStr;
}


- (instancetype) initAlertViewWithFrame:(CGRect)frame andSuperView:(UIView *)superView{
   
    if (self = [super initWithFrame:frame]) {
        isNeedCallBack = false;
        self.middleView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        [[UIApplication sharedApplication].keyWindow addSubview:self.middleView];

        // 中间区域
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 8;

        self.titleLabel.frame = CGRectMake(0, 10, self.frame.size.width, 30);
        [self addSubview:_titleLabel];
        
        // 内容
        self.linkLabel.font = [UIFont systemFontOfSize:14.f];
        self.linkLabel.textAlignment = NSTextAlignmentCenter;
        self.linkLabel.textInsets = UIEdgeInsetsMake(-10, 10, 0, 10);
        self.linkLabel.lineHeightMultiple = 0.9f;
        self.linkLabel.beforeAddLinkBlock = nil;
        self.linkLabel.dataDetectorTypes = MLDataDetectorTypeAll;
        self.linkLabel.allowLineBreakInsideLinks = YES;
        self.linkLabel.linkTextAttributes = nil;
        self.linkLabel.activeLinkTextAttributes = nil;
        [self.linkLabel invalidateDisplayForLinks];
        __weak typeof(self)weakSelf = self;
        [self.linkLabel setDidClickLinkBlock:^(MLLink *link, NSString *linkText, MLLinkLabel *label) {
            NSLog(@"linkText %@",linkText);
            [weakSelf dissMiss];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",linkText]]];
        }];
         [self addSubview:self.linkLabel];

    }
    return self;
    
}



- (UIButton *) creatButtonWithFrame:(CGRect) frame title:(NSString *) title{
    UIButton *cancelBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = frame;
    [cancelBtn setTitleColor:RGB(0, 123, 251,1) forState:UIControlStateNormal];
    [cancelBtn setTitle:title forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    return cancelBtn;
}


- (void)createTitle:(NSString * )title message:(NSString *)msg calcelButton:(NSString *)calBtn okButton:(NSString *)okBtn{
    
    if ([calBtn isEqualToString:@""] && [okBtn isEqualToString:@""]) {
        return;
    }
    // 标题
    self.titleLabel.text = title;
    
    // 内容
    NSMutableAttributedString* attrStr = [[NSMutableAttributedString alloc] initWithString:msg];
//    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(6, 15)];
//    [attrStr addAttribute:NSLinkAttributeName value:@"http://07568919527.locoso.com" range:NSMakeRange(6, 15)];
    self.linkLabel.attributedText = attrStr;
    
    //分割线
    UIView *horLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 43, self.frame.size.width, 0.5)];
    horLine.backgroundColor = RGB(213, 213, 215,1);
    [self addSubview:horLine];
    if (![calBtn isEqualToString:@""] && ![okBtn isEqualToString:@""]) {
        isNeedCallBack = true;
        // 两者都不为空
        UIView *vorLine = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width * 0.5, self.frame.size.height - 44, 1, 44)];
        vorLine.backgroundColor = RGB(213, 213, 215,1);
        [self addSubview:vorLine];
        
      CGRect  cancelBtnF = CGRectMake(0, self.frame.size.height - 42, self.frame.size.width * 0.5, 42);
        UIButton *cancelBtn = [self creatButtonWithFrame:cancelBtnF title:calBtn];
        [cancelBtn setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        [self addSubview:cancelBtn];
        [cancelBtn addTarget:self action:@selector(confirmBtnClick) forControlEvents:UIControlEventTouchUpInside];
        self.cancelButton = cancelBtn;
        
      CGRect  sureBtnF = CGRectMake(self.frame.size.width * 0.5, self.frame.size.height - 42, self.frame.size.width * 0.5, 42);
        UIButton *sureBtn = [self creatButtonWithFrame:sureBtnF title:okBtn];
        [self addSubview:sureBtn];
        [sureBtn addTarget:self action:@selector(confirmBtnClick) forControlEvents:UIControlEventTouchUpInside];
        return;
    }
    // 只存在一个按钮
    NSString * titleBtn = @"";
    if (![calBtn isEqualToString:@""] ) {
        titleBtn = calBtn;
    }else if(![okBtn isEqualToString:@""]){
        titleBtn = okBtn;
    }
    CGRect  confirmF = CGRectMake(0, self.frame.size.height - 42, self.frame.size.width, 42);
    UIButton *confirmBtn = [self creatButtonWithFrame:confirmF title:titleBtn];
    [self addSubview:confirmBtn];
    [confirmBtn addTarget:self action:@selector(confirmBtnClick) forControlEvents:UIControlEventTouchUpInside];
   
}


- (void) confirmBtnClick{
//    if (isNeedCallBack) {
//       
//    }
    if ([_delegate respondsToSelector:@selector(customAlertView:clickedButtonAtIndex:)]) {
        [_delegate customAlertView:self clickedButtonAtIndex:isNeedCallBack];
    }
    [self dissMiss];
}


#pragma mark - 注销视图
- (void) dissMiss{
    if (_middleView) {
        [_middleView removeFromSuperview];
        _middleView = nil;
    }
    [self removeFromSuperview];
}


@end

