//
//  YXCustomAlertView.h
//  YXCustomAlertView
//
//  Created by Houhua Yan on 16/7/12.
//  Copyright © 2016年 YanHouhua. All rights reserved.

//自定义的半透明弹窗

#import <UIKit/UIKit.h>

@class ACCustomAlertView;

@protocol ACCustomAlertViewDelegate <NSObject>

- (void) customAlertView:(ACCustomAlertView *) customAlertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@end


@interface ACCustomAlertView : UIView

/**
 *  遮罩层
 */
@property (nonatomic, strong) UIView *middleView;

@property(nonatomic, weak) id<ACCustomAlertViewDelegate> delegate;

/**
 * 弹窗在视图中的中心点
 **/
@property (nonatomic, assign) CGFloat centerY;

- (instancetype) initAlertViewWithFrame:(CGRect)frame andSuperView:(UIView *)superView;

- (void) createTitle:(NSString * )title message:(NSString *)msg calcelButton:(NSString *)calBtn okButton:(NSString *)okBtn;

@property(nonatomic,weak) UIButton * cancelButton;
/**
 *  移除视图
 */
- (void) dissMiss;

@end
