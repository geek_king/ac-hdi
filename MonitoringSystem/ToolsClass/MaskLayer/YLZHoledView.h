//
//  YLZHoledView.h
//  HoledViewTest
//
//  Created by Colin on 15/4/7.
//  Copyright (c) 2015年 icephone. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, YLZHoleType)
{
    YLZHoleTypeCirle,
    YLZHoleTypeRect,
    YLZHoleTypeRoundedRect,
    YLZHoleTypeCustomRect
};


@class YLZHoledView;
@protocol YLZHoledViewDelegate <NSObject>

- (void)holedView:(YLZHoledView *)holedView didSelectHoleAtIndex:(NSUInteger)index;

@end


@interface YLZHoledView : UIView

@property (strong, nonatomic) UIColor *dimingColor;
@property (weak, nonatomic) id <YLZHoledViewDelegate> holeViewDelegate;

- (NSInteger)addHoleCircleCenteredOnPosition:(CGPoint)centerPoint andDiameter:(CGFloat)diamter;
- (NSInteger)addHoleRectOnRect:(CGRect)rect;
- (NSInteger)addHoleRoundedRectOnRect:(CGRect)rect withCornerRadius:(CGFloat)cornerRadius;
- (NSInteger)addHCustomView:(UIView *)customView onRect:(CGRect)rect;

- (void)addFocusView:(UIView *)focus;

- (void)removeHoles;

/**
 *  init范围大小的时候，在当前所在委托的父类加入进去
 *
 */
- (instancetype)initWithFrame:(CGRect)frame withTagrget:(id) obj;

/**
 *   加入引导界面
 *
 *  @param showRect   需要引导的区域
 *  @param radius     引导的区域是否是圆形，如果是传入小于等于0的值就是矩形，反之就是圆形
 *  @param fingerRect 手指引导的区域
 *  @param hTag       回调到标示，根据holedView的tag来对应回调
 holedView:(YLZHoledView *)holedView didSelectHoleAtIndex:(NSUInteger)index
 */
- (void) initWithMaskLayerWithObj:(CGRect) showRect layerRadius:(NSInteger)radius fingerRect:(CGPoint) fingerRect holdViewTag:(NSInteger) hTag;
@end
