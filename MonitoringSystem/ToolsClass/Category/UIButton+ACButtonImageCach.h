//
//  UIButton+ACButtonImageCach.h
//  MonitoringSystem
//
//  Created by JiaKang.Zhong on 16/5/3.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

/**
 *
 *   此分类缓存按钮上的图片
 */
#import <UIKit/UIKit.h>

@interface UIButton (ACButtonImageCach)

+ (void) buttonWithImageCach:(UIButton *) obj andImageUrl:(NSString *)url;

@end
